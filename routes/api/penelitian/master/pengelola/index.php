<?php

Flight::route('GET /penelitian/master/pengelola', function () use ($mypdo) {
    $query = "SELECT * FROM peneliti_pengelola WHERE deleted_at IS NULL";
    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if ($result) {
        $response = array(
            'status' => 200,
            'message' => 'success',
            'data' => $result
        );
    } else {
        $response = array(
            'status' => 200,
            'message' => 'No data available',
        );
    }

    return Flight::json($response, $response['status']);
});

Flight::route('POST /penelitian/master/pengelola', function () use ($mypdo) {
    $request = Flight::request()->data;
    $today = date('Y-m-d H:i:s');

    try {
        $mypdo->beginTransaction();

        $query = "INSERT INTO peneliti_pengelola (
            pegawai_id,
            pegawai_nama,
            jenis,
            status,
            created_at,
            created_by
        ) VALUES (
            '$request->pegawai_id',
            '$request->pegawai_nama',
            '$request->jenis',
            '1',
            '$today',
            '$request->user_id'
        )";

        $stmt = $mypdo->prepare($query);
        $result = $stmt->execute();

        if ($result) {
            $response = array(
                'status' => 201,
                'message' => 'success'
            );
        } else {
            $response = array(
                'status' => 500,
                'message' => 'failed'
            );
        }

        $mypdo->commit();
    } catch (\Throwable $th) {
        $mypdo->rollback();

        $response = array(
            'status' => 500,
            'message' => 'database failed'
        );
    }

    return Flight::json($response, $response['status']);
});

Flight::route('GET /penelitian/master/pengelola/@id', function ($id) use ($mypdo) {
    $query = "SELECT * FROM peneliti_pengelola WHERE deleted_at IS NULL AND id = $id";
    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($result) {
        $response = array(
            'status' => 200,
            'message' => 'success',
            'data' => $result
        );
    } else {
        $response = array(
            'status' => 200,
            'message' => 'failed',
        );
    }

    return Flight::json($response, $response['status']);
});

Flight::route('PUT /penelitian/master/pengelola/@id', function ($id) use ($mypdo) {
    $request = json_decode(Flight::request()->getBody());
    $today = date('Y-m-d H:i:s');

    try {
        $mypdo->beginTransaction();

        $query = "UPDATE peneliti_pengelola 
            SET 
                pegawai_id = '$request->jenis_id',
                pegawai_nama = '$request->pendidikan_id',
                jenis = '$request->biaya',
                
                
                 = '$request->is_aktif',
                updated_at = '$today',
                updated_by = '$request->user_id'
            WHERE id = '$id'";

        $stmt = $mypdo->prepare($query);
        $result = $stmt->execute();

        $response = array(
            'status' => 200,
            'message' => 'success'
        );

        $mypdo->commit();
    } catch (\Throwable $th) {
        $mypdo->rollback();

        $response = array(
            'status' => 500,
            'message' => 'failed',
        );
    }

    return Flight::json($response, $response['status']);
});
