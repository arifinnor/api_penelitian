<?php

Flight::route('GET /penelitian/master/tarif', function () use ($mypdo) {
    $query = "SELECT * FROM peneliti_tarif WHERE deleted_at IS NULL";

    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    if (!$result) {
        $response = array(
            'status' => 204,
            'message' => 'No Data Available'
        );
    }

    return Flight::json($response, $response['status']);
});

Flight::route('POST /penelitian/master/tarif', function () use ($mypdo) {
    try {
        $mypdo->beginTransaction();

        $request = Flight::request()->data;
        $today = date('Y-m-d H:i:s');

        $query = "INSERT INTO peneliti_tarif (
                jenis_id,
                pendidikan_id,
                biaya,
                created_at,
                created_by
            ) VALUES (
                '$request->jenis_id',
                '$request->pendidikan_id',
                '$request->biaya',
                '$today',
                '$request->user_id'
            )";

        $stmt = $mypdo->prepare($query);
        $result = $stmt->execute();

        $response = array(
            'status' => 201,
            'message' => 'success'
        );

        $mypdo->commit();
    } catch (\Throwable $th) {
        $mypdo->rollback();

        $response = array(
            'status' => 500,
            'message' => 'failed',
        );
    }



    return Flight::json($response, $response['status']);
});

Flight::route('GET /penelitian/master/tarif/@id', function ($id) use ($mypdo) {

    $query = "SELECT * FROM peneliti_tarif WHERE id in ($id)";

    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    if (!$result) {
        $response = array(
            'status' => 204,
            'message' => 'No Data Available'
        );
    }

    return Flight::json($response, $response['status']);
});

Flight::route('PUT /penelitian/master/tarif/@id', function ($id) use ($mypdo) {
    $request = json_decode(Flight::request()->getBody());
    $today = date('Y-m-d H:i:s');

    try {
        $mypdo->beginTransaction();

        $query = "UPDATE peneliti_tarif 
            SET 
                jenis_id = '$request->jenis_id',
                pendidikan_id = '$request->pendidikan_id',
                biaya = '$request->biaya',
                is_aktif = '$request->is_aktif',
                updated_at = '$today',
                updated_by = '$request->user_id'
            WHERE id = '$id'";

        $stmt = $mypdo->prepare($query);
        $result = $stmt->execute();

        $response = array(
            'status' => 200,
            'message' => 'success'
        );

        $mypdo->commit();
    } catch (\Throwable $th) {
        $mypdo->rollback();

        $response = array(
            'status' => 500,
            'message' => 'failed',
        );
    }

    return Flight::json($response, $response['status']);
});
