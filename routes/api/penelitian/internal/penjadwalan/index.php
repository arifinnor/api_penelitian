<?php

Flight::route('GET /penelitian/internal/penjadwalan', function () use ($mypdo) {
    $query = "SELECT * FROM peneliti_internal_jadwal WHERE deleted_at IS NULL";
    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    if (!$result) {
        $response = array(
            'status' => 204,
            'message' => 'No Content'
        );
    }

    return Flight::json($response, $response['status']);
});

Flight::route('POST /penelitian/internal/penjadwalan', function () use ($mypdo) {
    try {
        $mypdo->beginTransaction();

        $request = Flight::request()->data;

        if (
            empty($request->judul) ||
            empty($request->kategori) ||
            empty($request->anggaran) ||
            empty($request->tanggal_mulai) ||
            empty($request->tanggal_selesai) ||
            empty($request->member)
        ) {
            throw new \Exception('Data tidak lengkap', 400);
        }

        $today = date('Y-m-d H:i:s');

        $query = "INSERT INTO peneliti_internal_jadwal (
                judul,
                kategori,
                anggaran,
                tanggal_mulai,
                tanggal_selesai,
                created_at,
                created_by
            ) VALUES (
                '$request->judul',
                '$request->kategori',
                '$request->anggaran',
                '$request->tanggal_mulai',
                '$request->tanggal_selesai',
                '$today',
                $request->user_id
            )";

        $stmt = $mypdo->prepare($query);
        $stmt->execute();

        $latest = $mypdo->lastInsertId();

        foreach ($request->member as $member) {
            $query2 = "INSERT INTO peneliti_internal_member (
                penelitian_id,
                pegawai_id,
                pegawai_nama,
                created_at,
                created_by
            ) VALUES (
                '$latest',
                '{$member['pegawai_id']}',
                '{$member['pegawai_nama']}',
                '$today',
                $request->user_id
            )";

            $stmt2 = $mypdo->prepare($query2);
            $stmt2->execute();
        }

        if (!empty($request->file) && isset($request->file)) {

            foreach ($request->file as $file) {
                $query3 = "INSERT INTO peneliti_internal_file (
                    penelitian_id,
                    nama,
                    path,
                    created_at,
                    created_by
                ) VALUES (
                    '$latest',
                    '{$file['nama']}',
                    '{$file['path']}',
                    '$today',
                    $request->user_id
                )";

                $stmt3 = $mypdo->prepare($query3);
                $stmt3->execute();
            }
        }

        $response = array(
            'status' => 201,
            'message' => 'success'
        );

        $mypdo->commit();
    } catch (\Exception $e) {
        $mypdo->rollback();

        $response = array(
            'status' => 500,
            'message' => 'failed',
            'error' => $e->getMessage(),
        );
    }

    return Flight::json($response, $response['status']);
});

Flight::route('GET /penelitian/internal/penjadwalan/@id', function ($id) use ($mypdo) {
    $data = array();

    $query = "SELECT * FROM peneliti_internal_jadwal j WHERE j.id IN ($id)";
    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($result) {
        $data = $result;

        $query2 = "SELECT * FROM peneliti_internal_member m WHERE m.penelitian_id IN ($id)";
        $stmt2 = $mypdo->prepare($query2);
        $stmt2->execute();
        $result2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);

        if ($result2) {
            $data['member'] = $result2;
        } else {
            $data['member'] = array();
        }

        $query3 = "SELECT * FROM peneliti_internal_file f WHERE f.penelitian_id IN ($id)";
        $stmt3 = $mypdo->prepare($query3);
        $stmt3->execute();
        $result3 = $stmt3->fetchAll(PDO::FETCH_ASSOC);

        if ($result3) {
            $data['file'] = $result3;
        } else {
            $data['file'] = array();
        }

        $response = array(
            'status' => 200,
            'message' => 'success',
            'data' => $data
        );
    } else {
        $response = array(
            'status' => 204,
            'message' => 'No Content'
        );
    }

    return Flight::json($response, $response['status']);
});

Flight::route('PUT /penelitian/internal/penjadwalan/@id', function ($id) use ($mypdo) {
    $request = json_decode(Flight::request()->getBody(), TRUE);
    $today = date('Y-m-d H:i:s');

    try {
        $mypdo->beginTransaction();

        if (
            empty($request['judul']) ||
            empty($request['kategori']) ||
            empty($request['anggaran']) ||
            empty($request['tanggal_mulai']) ||
            empty($request['tanggal_selesai'])
        ) {
            throw new \Exception('Data tidak lengkap', 400);
        }

        $query = "UPDATE 
                    peneliti_internal_jadwal 
                SET
                    judul = '{$request['judul']}',
                    kategori = '{$request['kategori']}',
                    anggaran = '{$request['anggaran']}',
                    tanggal_mulai = '{$request['tanggal_mulai']}',
                    tanggal_selesai = '{$request['tanggal_selesai']}',
                    is_aktif = '{$request['is_aktif']}',
                    updated_at = '{$today}',
                    updated_by = {$request['user_id']}
            WHERE id = $id";

        $stmt = $mypdo->prepare($query);
        $stmt->execute();

        if (!empty($request['member'])) {
            $mypdo->exec("DELETE FROM peneliti_internal_member WHERE penelitian_id = $id");

            foreach ($request['member'] as $member) {
                $query2 = "INSERT INTO peneliti_internal_member (
                        penelitian_id,
                        pegawai_id,
                        pegawai_nama,
                        created_at,
                        created_by
                    ) VALUES (
                        '$id',
                        '{$member['pegawai_id']}',
                        '{$member['pegawai_nama']}',
                        '$today',
                        '{$request['user_id']}'
                    )";

                $stmt2 = $mypdo->prepare($query2);
                $stmt2->execute();
            }
        }

        if (!empty($request['file'])) {
            $mypdo->exec("DELETE FROM peneliti_internal_file WHERE penelitian_id = $id");

            foreach ($request['file'] as $file) {
                $query3 = "INSERT INTO peneliti_internal_file (
                    penelitian_id,
                    nama,
                    path,
                    created_at,
                    created_by
                ) VALUES (
                    $id,
                    '{$file['nama']}',
                    '{$file['path']}',
                    '$today',
                    {$request['user_id']}
                )";

                $stmt3 = $mypdo->prepare($query3);
                $stmt3->execute();
            }
        }

        $mypdo->commit();

        $response = array(
            'status' => 200,
            'message' => 'success'
        );
    } catch (\Exception $e) {
        $mypdo->rollback();

        $response = array(
            'status' => 500,
            'message' => 'failed',
            'error' => $e->getMessage()
        );
    }

    return Flight::json($response, $response['status']);
});

Flight::route('DELETE /penelitian/internal/penjadwalan/@id', function ($id) use ($mypdo) {
    $request = Flight::request()->data;

    $userId = $request['user_id'];
    $today = date('Y-m-d H:i:s');

    $query = "UPDATE peneliti_internal_jadwal 
            SET deleted_at = '$today', deleted_by = '$userId' 
        WHERE id = $id
    ";

    $stmt = $mypdo->prepare($query);
    $stmt->execute();

    $response = array(
        'status' => 200,
        'message' => 'success'
    );

    return Flight::json($response, $response['status']);
});
