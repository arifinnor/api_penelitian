<?php
Flight::route('GET /master/tarif', function () use ($db) {
  $sk = Flight::request()->query->no_sk;
  $prodi = Flight::request()->query->prodi;
  $tahun = Flight::request()->query->tahun;

  $query = "SELECT * FROM m_tarif";

  if ($sk && $prodi && $tahun) {
    $query .= " WHERE no_sk='{$sk}' AND prodi_id='{$prodi}' AND tahun_sk='{$tahun}'";
  } else {
    $query .= " GROUP BY prodi_id, no_sk, tahun_sk ORDER BY id DESC ";
  }

  $stmt = $db->prepare($query);
  $stmt->execute();

  $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
  if ($result) {
    $response = array(
      'success' => true,
      'status' => 200,
      'data' => $result
    );
  } else {
    $response = array(
      'success' => false,
      'status' => 404,
      'message' => 'Data not found'
    );
  }


  Flight::json($response, $response['status']);
});

Flight::route('POST /master/tarif', function () use ($db) {
  $request = Flight::request()->data;
  $date = date('Y-m-d H:i:s');
  try {
    $db->beginTransaction();

    foreach ($request as $data) {
      if (empty($data['prodi_id'])) {
        throw new Exception("Nama Tarif tidak boleh kosong.", 422);
      }

      if (empty($data['nama_tarif'])) {
        throw new Exception("Nama tarif tidak boleh kosong.", 422);
      }

      if (empty($data['no_sk'])) {
        throw new Exception("Nomor SK tarif tidak boleh kosong.", 422);
      }

      if (empty($data['tahun_sk'])) {
        throw new Exception("Tahun SK tarif tidak boleh kosong.", 422);
      }

      if (empty($data['user_id'])) {
        throw new Exception("User ID tidak ditemukan.", 422);
      }

      $querySk = "SELECT 
          COUNT(no_sk) 
        FROM 
          m_tarif 
          WHERE no_sk = '{$data['no_sk']}' 
            AND prodi_id = '{$data['prodi_id']}'
            AND jenis_tarif = '{$data['jenis_tarif']}'
      ";

      $stmt = $db->query($querySk);
      $row = $stmt->fetchColumn();

      if ($row > 0) {
        throw new Exception('Tarif program studi dengan nomor SK ini sudah ada.', 422);
      }

      if ($data['jenis_tarif'] == 1 && empty($data['nilai_tarif'])) {
        throw new Exception("Nilai tarif praktek tidak boleh kosong.", 422);
      }

      if ($data['jenis_tarif'] == 2 && empty($data['nilai_tarif'])) {
        throw new Exception("Nilai tarif ujian tidak boleh kosong.", 422);
      }

      if ($data['jenis_tarif'] == 3 && empty($data['nilai_tarif'])) {
        throw new Exception("Nilai tarif kunjungan tidak boleh kosong.", 422);
      }

      $query = "INSERT INTO m_tarif (
              prodi_id,
              nama_tarif, 
              jenis_tarif, 
              nilai_tarif, 
              satuan_tarif, 
              no_sk, 
              tahun_sk,
              status, 
              created_at, 
              created_by
          ) VALUES (
              '$data[prodi_id]',
              '$data[nama_tarif]',
              '$data[jenis_tarif]',
              '$data[nilai_tarif]',
              '$data[satuan_tarif]',
              '$data[no_sk]',
              '$data[tahun_sk]',
              '$data[status]',
              '$date',
              '$data[user_id]'
          )";

      $stmt = $db->prepare($query);
      $result = $stmt->execute();
    }

    if ($result) {
      $response = array(
        'success' => true,
        'status' => 201,
        'message' => 'Data berhasil disimpan.'
      );
    } else {
      $response = array(
        'success' => false,
        'status' => 500,
        'message' => 'Internal Server error.'
      );
    }

    $db->commit();
  } catch (\Exception $e) {
    $db->rollback();

    $response =  array(
      'success' => false,
      'status' => $e->getCode(),
      'message' => $e->getMessage()
    );
  }

  Flight::json($response, $response['status']);
});

Flight::route('GET /master/tarif/@id', function ($id) use ($db) {
  $query = "SELECT * FROM m_tarif WHERE id = {$id}";
  $stmt = $db->prepare($query);
  $stmt->execute();
  $result = $stmt->fetch(PDO::FETCH_ASSOC);

  if ($result) {
    $response = array(
      'success' => true,
      'status' => 200,
      'data' => $result
    );
  } else {
    $response = array(
      'success' => false,
      'status' => 404,
      'message' => 'Data not found.'
    );
  }

  Flight::json($response, $response['status']);
});

Flight::route('PUT /master/tarif', function () use ($db) {
  $request = json_decode(Flight::request()->getBody(), TRUE);
  $date = date('Y-m-d H:i:s');

  try {
    $db->beginTransaction();

    foreach ($request as $data) {
      if (empty($data['prodi_id'])) {
        throw new Exception("Nama Tarif tidak boleh kosong.", 422);
      }

      if (empty($data['nama_tarif'])) {
        throw new Exception("Nama tarif tidak boleh kosong.", 422);
      }

      if (empty($data['no_sk'])) {
        throw new Exception("Nomor SK tarif tidak boleh kosong.", 422);
      }

      if (empty($data['tahun_sk'])) {
        throw new Exception("Tahun SK tarif tidak boleh kosong.", 422);
      }

      if (empty($data['user_id'])) {
        throw new Exception("User ID tidak ditemukan.", 422);
      }

      if ($data['jenis_tarif'] == 1 && empty($data['nilai_tarif'])) {
        throw new Exception("Nilai tarif praktek tidak boleh kosong.", 422);
      }

      if ($data['jenis_tarif'] == 2 && empty($data['nilai_tarif'])) {
        throw new Exception("Nilai tarif ujian tidak boleh kosong.", 422);
      }

      if ($data['jenis_tarif'] == 3 && empty($data['nilai_tarif'])) {
        throw new Exception("Nilai tarif kunjungan tidak boleh kosong.", 422);
      }

      $query = "UPDATE m_tarif
                  SET
                      nilai_tarif = '{$data['nilai_tarif']}',
                      satuan_tarif = '{$data['satuan_tarif']}',
                      updated_at = '$date',
                      updated_by = '{$data['user_id']}'
              WHERE prodi_id = '{$data['prodi_id']}' 
                AND no_sk = '{$data['no_sk']}' 
                AND tahun_sk = '{$data['tahun_sk']}'  
                AND jenis_tarif = '{$data['jenis_tarif']}' 
          ";

      $stmt = $db->prepare($query);
      $result = $stmt->execute();
    }

    if ($result) {
      $response = array(
        'success' => true,
        'status' => 201,
        'message' => 'Data berhasil disimpan.'
      );
    } else {
      $response = array(
        'success' => false,
        'status' => 500,
        'message' => 'Internal Server error.'
      );
    }

    $db->commit();
  } catch (\Exception $e) {
    $db->rollback();

    $response =  array(
      'success' => false,
      'status' => 500,
      'message' => $e->getMessage()
    );
  }

  Flight::json($response, $response['status']);
});
