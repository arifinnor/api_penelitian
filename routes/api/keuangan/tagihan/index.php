<?php

Flight::route('GET /keuangan/tagihan/peserta_didik/praktek/@institusi/@prodi/@departemen', function ($institusi, $prodi, $departemen) use ($db) {
    $query = "SELECT
				  jp.`peserta_didik_id`,
				  jp.`departemen_id`,
				  pd.`nama` AS peserta_didik_nama,
				  pd.`nim` AS peserta_didik_nim,
				  dp.`nama` AS departemen_nama,
				  MIN(jp.`tgl_mulai`) AS jadwal_tgl_mulai,
				  MAX(jp.`tgl_selesai`) AS jadwal_tgl_selesai,
				  praktek.nilai_tarif AS biaya_praktek,
                  COALESCE(jumlah_ujian.jumlah, 0) AS jumlah_ujian,
				  ujian.nilai_tarif AS biaya_ujian
				FROM
				  t_jadwal_praktek jp
				  INNER JOIN m_peserta_didik pd
					ON pd.`id` = jp.`peserta_didik_id`
				  INNER JOIN m_departemen dp
					ON dp.`id` = jp.`departemen_id`
				  LEFT JOIN
					(SELECT DISTINCT
					  tarif.*
					FROM
					  m_tarif tarif
					  INNER JOIN m_prodi prodi
						ON prodi.`id` = tarif.`prodi_id`
					  INNER JOIN m_peserta_didik siswa
						ON siswa.`prodi_id` = prodi.`id`
					WHERE tarif.`jenis_tarif` = 3) praktek
					ON praktek.prodi_id = pd.`prodi_id`
				  LEFT JOIN
					(SELECT DISTINCT
					  tarif.*
					FROM
					  m_tarif tarif
					  INNER JOIN m_prodi prodi
						ON prodi.`id` = tarif.`prodi_id`
					  INNER JOIN m_peserta_didik siswa
						ON siswa.`prodi_id` = prodi.`id`
					WHERE tarif.`jenis_tarif` = 4) ujian
					ON ujian.prodi_id = pd.`prodi_id`
				  LEFT JOIN
					(SELECT COUNT(a.peserta_didik_id) AS jumlah, a.peserta_didik_id
                        FROM t_penilaian a
                        WHERE a.jenis_penilaian = '4'
                            AND a.departemen_id = '{$departemen}'
                        GROUP BY a.`peserta_didik_id`) jumlah_ujian
					ON jumlah_ujian.peserta_didik_id = jp.peserta_didik_id
				WHERE jp.`status` = '1'
				  AND pd.`institusi_id` = '{$institusi}'
				  AND pd.`prodi_id` = '{$prodi}'
				  AND jp.`departemen_id` = '{$departemen}'
				  AND jp.peserta_didik_id NOT IN
				  (SELECT detail_peserta.`peserta_didik_id`
                   FROM t_tagihan_detail detail_peserta
                   WHERE departemen_id <> '{$departemen}'
                     AND detail_peserta.peserta_didik_id <> jp.peserta_didik_id)
				GROUP BY `peserta_didik_id`,
				  `peserta_didik_nama`,
				  `peserta_didik_nim`,
				  `departemen_nama`
				ORDER BY pd.`nama`,
				  dp.`nama` ASC
    ";

    $stmt = $db->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if ($result) {

        $response = array(
            'success' => true,
            'status' => 200,
            'data' => $result
        );
    } else {
        $response = array(
            'success' => false,
            'status' => 404,
            'message' => 'Data not found.'
        );
    }

    Flight::json($response);
});

Flight::route('GET /keuangan/tagihan/peserta_didik/smk/@institusi/@prodi/@departemen', function ($institusi, $prodi, $departemen) use ($db) {
    $query = "SELECT
            jp.`id` AS jadwal_id,
            jp.`departemen_id`,
            jp.`peserta_didik_id`,
            siswa.`nama` AS peserta_didik_nama,
            siswa.`nim` AS peserta_didik_nim,
            d.`nama` AS departemen_nama,
            jp.`tgl_mulai` AS jadwal_tgl_mulai,
            jp.`tgl_selesai` AS jadwal_tgl_selesai,
            praktek.nilai_tarif AS biaya_praktek,
            CEIL(IF(DATEDIFF(DATE (jp.`tgl_selesai`), DATE (jp.`tgl_mulai`))>0,
                DATEDIFF(DATE (jp.`tgl_selesai`), DATE (jp.`tgl_mulai`)), 1)/7) AS jumlah_minggu
        FROM
            t_jadwal_praktek jp
            INNER JOIN m_peserta_didik siswa
            ON siswa.`id` = jp.`peserta_didik_id`
            INNER JOIN m_departemen d
            ON d.`id` = jp.`departemen_id`
            LEFT JOIN
            (SELECT DISTINCT
                tarif.*
            FROM
                m_tarif tarif
                INNER JOIN m_prodi prodi
                ON prodi.`id` = tarif.`prodi_id`
                INNER JOIN m_peserta_didik siswa
                ON siswa.`prodi_id` = prodi.`id`
            WHERE tarif.`jenis_tarif` = '3') praktek
            ON praktek.prodi_id = siswa.`prodi_id`
        WHERE jp.`status` = '1'
            AND siswa.`institusi_id` = '{$institusi}'
            AND siswa.`prodi_id` = '{$prodi}'
            AND jp.`departemen_id` = '{$departemen}'
            AND jp.`peserta_didik_id` NOT IN
            (SELECT
            `peserta_didik_id`
            FROM
            t_tagihan_detail WHERE departemen_id <> '{$departemen}')
        GROUP BY jp.`peserta_didik_id`, jp.`departemen_id`
    ";

    $stmt = $db->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if ($result) {
        $response = array(
            'success' => true,
            'status' => 200,
            'data' => $result
        );
    } else {
        $response = array(
            'success' => false,
            'status' => 404,
            'message' => 'Data not found.'
        );
    }

    Flight::json($response);
});

Flight::route('GET /keuangan/tagihan', function () use ($db) {
    $query = "SELECT DISTINCT
            t.*, 
            i.nama AS institusi_nama, 
            d.nama AS departemen_nama,
            UPPER(CONCAT(p.jenjang, ' - ', p.nama)) AS prodi_nama 
        FROM t_tagihan_header t
            INNER JOIN t_tagihan_detail det ON det.tagihan_id = t.id
            INNER JOIN m_peserta_didik pd ON pd.id = det.peserta_didik_id  
            INNER JOIN m_institusi i ON i.id = t.institusi_id 
            INNER JOIN m_departemen d ON d.id = t.departemen_id
            INNER JOIN m_prodi p ON p.id = pd.prodi_id
        ORDER BY t.id DESC, t.created_at DESC";

    $stmt = $db->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if ($result) {
        $response = array(
            'success' => true,
            'status' => 200,
            'data' => $result
        );
    } else {
        $response = array(
            'success' => false,
            'status' => 404,
            'message' => 'Data not found.'
        );
    }

    Flight::json($response);
});

Flight::route('GET /keuangan/tagihan/@id', function ($id) use ($db) {
    $stmt1 = $db->prepare("SELECT * FROM t_tagihan_header WHERE id = '{$id}'");
    $stmt1->execute();

    $result1 = $stmt1->fetch(PDO::FETCH_ASSOC);

    $query = "SELECT
                t.`id`,
                t.`tagihan_id`,
                t.`peserta_didik_id`,
                d.`nama` AS departemen_nama,
                s.`nama` AS peserta_didik_nama,
                s.`nim` AS peserta_didik_nim,
                t.`tgl_mulai` AS jadwal_tgl_mulai,
                t.`tgl_selesai` AS jadwal_tgl_selesai,
                t.`lama_pendidikan`,
                COALESCE(jumlah_ujian, 0) AS jumlah_ujian,
                t.`biaya_praktek`,
                t.`biaya_ujian`,
                ujian.`nilai_tarif` AS tarif_ujian
            FROM
                t_tagihan_detail t
                INNER JOIN m_peserta_didik s ON s.`id` = t.`peserta_didik_id`
                INNER JOIN m_departemen d ON d.`id` = t.`departemen_id`
                LEFT JOIN (SELECT DISTINCT tarif.* FROM m_tarif tarif
                            INNER JOIN m_prodi prodi ON prodi.`id` = tarif.`prodi_id`
                            INNER JOIN m_peserta_didik siswa ON siswa.`prodi_id` = prodi.`id`
                            WHERE tarif.`jenis_tarif` = 4) ujian
                ON ujian.prodi_id = s.`prodi_id`
            WHERE tagihan_id = '{$id}'
            ORDER BY s.nama";

    $stmt2 = $db->prepare($query);
    $stmt2->execute();

    $result2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);

    $result1['detail'] = $result2;

    if ($result1) {
        $response = array(
            'success' => true,
            'status' => 200,
            'data' => $result1
        );
    } else {
        $response = array(
            'success' => false,
            'status' => 404,
            'message' => 'Data not found.'
        );
    }

    Flight::json($response, $response['status']);
});

Flight::route('POST /keuangan/tagihan', function () use ($db) {
    $request = Flight::request()->data;
    $today = date('Y-m-d H:i:s');

    try {
        $db->beginTransaction();

        if (empty($request->jenis_tagihan)) {
            throw new Exception('Jenis tagihan harus diisi.', 422);
        }

        if (empty($request->kode_tagihan)) {
            throw new Exception('Kode tagihan harus diisi.', 422);
        }

        if (empty($request->institusi_id)) {
            throw new Exception('Institusi tidak ditemukan', 422);
        }

        if (empty($request->nilai_tagihan)) {
            throw new Exception('Nilai tagihan harus diisi.', 422);
        }

        $queryHeader = "INSERT INTO 
                t_tagihan_header 
                (
                    institusi_id,
                    prodi_id,
                    departemen_id,
                    tgl_tagihan, 
                    kode_tagihan,
                    is_bayar, 
                    is_verif, 
                    nilai_tagihan, 
                    keterangan,
                    jenis_tagihan, 
                    created_at, 
                    created_by
                ) VALUES (
                    '$request->institusi_id',
                    '$request->prodi_id',
                    '$request->departemen_id',
                    '$request->tgl_tagihan',
                    '$request->kode_tagihan',
                    '$request->is_bayar',
                    '$request->is_verif',
                    '$request->nilai_tagihan',
                    '$request->keterangan',
                    '$request->jenis_tagihan',
                    '$today', 
                    '$request->created_by'
                )
        ";

        $db->exec($queryHeader);
        $latest = $db->lastInsertId();

        if (!$latest) {
            throw new Exception('Gagal menyimpan tagihan.', 500);
        }

        foreach ($request['detail'] as $data) {
            if (empty($latest)) {
                throw new Exception('Tagihan tidak ditemukan.', 422);
            }

            if (empty($data['peserta_didik_id'])) {
                throw new Exception('Peserta didik tidak ditemukan.', 422);
            }

            $queryDetail = "INSERT INTO 
                t_tagihan_detail (
                    tagihan_id,
                    peserta_didik_id,
                    departemen_id,
                    tgl_mulai,
                    tgl_selesai,
                    lama_pendidikan,
                    biaya_praktek,
                    jumlah_ujian,
                    biaya_ujian,
                    total_biaya,
                    created_at,
                    created_by
                ) VALUES (
                    '{$latest}',
                    '{$data['peserta_didik_id']}',
                    '{$data['departemen_id']}',
                    '{$data['tgl_mulai']}',
                    '{$data['tgl_selesai']}',
                    '{$data['lama_pendidikan']}',
                    '{$data['biaya_praktek']}',
                    '{$data['jumlah_ujian']}',
                    '{$data['biaya_ujian']}',
                    '{$data['total_biaya']}',
                    '{$today}',
                    '{$data['created_by']}'
                )
            ";

            $result = $db->exec($queryDetail);
        }

        if ($result) {
            $response = array(
                'success' => true,
                'status' => 201,
                'message' => 'Data berhasil disimpan.',
            );
        } else {
            throw new Exception('Gagal menyimpan detail tagihan.', 500);
        }

        $db->commit();
    } catch (Exception $e) {
        $response = array(
            'success' => false,
            'status' => 500,
            'message' => $e->getMessage(),
        );

        $db->rollback();
    }

    Flight::json($response, $response['status']);
});

Flight::route('PUT /keuangan/tagihan/@id', function ($id) use ($db) {
    $request = json_decode(Flight::request()->getBody(), TRUE);
    $today = date('Y-m-d H:i:s');

    try {
        $db->beginTransaction();

        $query = "UPDATE 
                t_tagihan_header 
            SET
                kode_tagihan = '{$request['kode_tagihan']}',
                tgl_tagihan = '{$request['tgl_tagihan']}',
                keterangan = '{$request['keterangan']}',
                updated_at = '{$today}',
                updated_by = '{$request['updated_by']}'
            WHERE id = '{$id}'
        ";

        $stmt1 = $db->prepare($query);
        $result = $stmt1->execute();

        if ($result) {
            $response = array(
                'success' => true,
                'status' => 200,
                'message' => 'Data berhasil disimpan.',
            );
        }

        $db->commit();
    } catch (\Exception $e) {
        $response = array(
            'success' => false,
            'status' => $e->getCode(),
            'message' => $e->getMessage(),
        );

        $db->rollback();
    }

    Flight::json($response, $response['status']);
});

Flight::route('PUT /keuangan/tagihan/batal/@id', function ($id) use ($db) {
    $request = json_decode(Flight::request()->getBody(), TRUE);
    $today = date('Y-m-d H:i:s');

    try {
        $db->beginTransaction();

        $query = "UPDATE 
                t_tagihan_header 
            SET
                status = '0',
                updated_at = '{$today}',
                updated_by = '{$request['updated_by']}'
            WHERE id = '{$id}'
        ";

        $stmt1 = $db->prepare($query);
        $result = $stmt1->execute();

        if ($result) {
            $response = array(
                'success' => true,
                'status' => 200,
                'message' => 'Data berhasil disimpan.',
            );
        }

        $db->commit();
    } catch (\Exception $e) {
        $response = array(
            'success' => false,
            'status' => $e->getCode(),
            'message' => $e->getMessage(),
        );

        $db->rollback();
    }

    Flight::json($response, $response['status']);
});

Flight::route('DELETE /keuangan/tagihan/@id', function ($id) use ($db) {
    $request = json_decode(Flight::request()->getBody(), TRUE);
    $today = date('Y-m-d H:i:s');

    try {
        $db->beginTransaction();

        $queryDetail = "DELETE FROM t_tagihan_detail WHERE tagihan_id = '{$id}'";

        $stmtDetail = $db->prepare($queryDetail);
        $resultDetail = $stmtDetail->execute();

        if ($resultDetail) {
            $queryHeader = "DELETE FROM t_tagihan_header WHERE id = '{$id}'";

            $stmtHeader = $db->prepare($queryHeader);
            $stmtHeader->execute();
        }

        $response = array(
            'success' => true,
            'status' => 200,
            'message' => 'Data berhasil dihapus.',
        );

        $db->commit();
    } catch (\Exception $e) {
        $response = array(
            'success' => false,
            'status' => 500,
            'message' => $e->getMessage(),
        );

        $db->rollback();
    }

    Flight::json($response, $response['status']);
});
