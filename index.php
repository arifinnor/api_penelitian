<?php
require 'vendor/autoload.php';
// require '/flight/Flight.php';
require '/helpers/databases.php';

$db = Flight::db(false);
$mypdo = Flight::db2(false);

date_default_timezone_set('Asia/Jakarta');

if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: *");
    //header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    exit(0);
}

// Index
Flight::route('/', function () {
    echo "Hello World";
});

// ! ============================================================================
// ! Route Untuk Data Peneliti
// ! ============================================================================

/* Get All Data Peneliti */
Flight::route('GET /peneliti', function () use ($mypdo) {
    $query = "SELECT id, nik, nama_peneliti, telepon, email,
            instansi, status_peneliti FROM peneliti ORDER BY id DESC";

    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    if (!$result) {
        $response = array(
            'status' => 404,
            'message' => 'No Data Available',
        );
    }

    Flight::json($response);
});

/* Get data by id Peneliti */
Flight::route('GET /peneliti/@id', function ($id) use ($mypdo) {
    $query = "SELECT * FROM peneliti WHERE id = $id";
    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    if (!$result) {
        $response = array(
            'status' => 404,
            'message' => 'No Data Available',
        );
    }

    Flight::json($response);
});

/* Store Data Peneliti */
Flight::route('POST /peneliti', function () use ($mypdo) {
    $request = Flight::request()->data;

    $nik = $request->nik;
    $nama = $request->nama;
    $telepon = $request->telepon;
    $email = $request->email;
    $pob = $request->pob;
    $dob = $request->dob;
    $sex = $request->sex;
    $pendidikan = $request->pendidikan;
    $instansi = $request->instansi;
    $fakultas = $request->fakultas;
    $jurusan = $request->jurusan;
    $status = $request->statusPeneliti;
    $date = date('Y-m-d H:i:s');

    if (
        empty($nik) or
        empty($nama) or
        empty($telepon) or
        empty($email)
    ) {
        $response = array(
            'status' => 403,
            'message' => 'Failed, your data is not complete.'
        );

        Flight::json($response);
        die();
        // Flight::halt($response);
    }

    $query = "INSERT INTO peneliti (
            nik,
            nama_peneliti,
            telepon,
            email,
            pob,
            dob,
            sex,
            pendidikan,
            instansi,
            fakultas,
            jurusan,
            status_peneliti,
            created_at
        )
        VALUES (
            '$nik',
            '$nama',
            '$telepon',
            '$email',
            '$pob',
            '$dob',
            '$sex',
            '$pendidikan',
            '$instansi',
            '$fakultas',
            '$jurusan',
            '$status',
            '$date'

        )";

    $stmt = $mypdo->prepare($query);
    $result = $stmt->execute();

    if ($result) {
        $response = array(
            'status' => 201,
            'message' => 'Data has been added.'
        );
    } else {
        $response = array(
            'status' => 204,
            'message' => 'Failed to save the data.'
        );
    }

    Flight::json($response);
});

/* Update Data Peneliti */
Flight::route('PUT /peneliti/@id', function ($id) use ($mypdo) {
    $request = json_decode(Flight::request()->getBody());

    $nik = $request->nik;
    $nama = $request->nama;
    $telepon = $request->telepon;
    $email = $request->email;
    $pob = $request->pob;
    $dob = $request->dob;
    $sex = $request->sex;
    $pendidikan = $request->pendidikan;
    $instansi = $request->instansi;
    $fakultas = $request->fakultas;
    $jurusan = $request->jurusan;
    $status = $request->statusPeneliti;
    $date = date('Y-m-d H:i:s');

    $query = "UPDATE
            peneliti
        SET
            nik = '$nik',
            nama_peneliti = '$nama',
            telepon = '$telepon',
            email = '$email',
            pob = '$pob',
            dob = '$dob',
            sex = '$sex',
            pendidikan = '$pendidikan',
            instansi = '$instansi',
            fakultas = '$fakultas',
            jurusan = '$jurusan',
            status_peneliti = '$status',
            updated_at = '$date'
        WHERE
            id = $id
    ";

    $stmt = $mypdo->prepare($query);
    $result = $stmt->execute();

    if ($result) {
        $response = array(
            'status' => 200,
            'message' => 'Data has been updated.'
        );
    } else {
        $response = array(
            'status' => 204,
            'message' => 'Failed to update the data.'
        );
    }

    Flight::json($response);
});

/* Delete Data Peneliti */
Flight::route('DELETE /peneliti/@id', function ($id) use ($mypdo) {
    $query = "DELETE FROM peneliti WHERE id=$id";
    $stmt = $mypdo->prepare($query);
    $stmt->execute();

    $response = array(
        'status' => 200,
        'message' => 'Data has been deleted.'
    );

    Flight::json($response);
});

/* Soft Delete Data Peneliti */
Flight::route('POST /peneliti/recycle/@id', function ($id) use ($mypdo) {
    $date = date('Y-m-d H:i:s');

    $query = "UPDATE peneliti SET delete_at = '$date' WHERE id = '$id'";

    $stmt = $mypdo->prepare($query);
    $result = $stmt->execute();

    if ($result) {
        $response = array(
            'status' => 200,
            'message' => 'Data has been sent to the trash.'
        );
    } else {
        $response = array(
            'status' => 204,
            'message' => 'Failed to delete the data.'
        );
    }

    Flight::json($response);
});

//  !================== End Data Peneliti =====================================

// ! ==========================================================================
// ! Route untuk file handling
// ! ==========================================================================

// Store
Flight::route('POST /peneliti/@nik/berkas/', function ($nik) use ($mypdo) {
    $files = Flight::request()->files->berkas;

    $nik = Flight::request()->data->nik;
    $kdFile = Flight::request()->data->kdFile;

    $base = Flight::request()->base; // return "/api_penelitian"
    $host = Flight::request()->host; // return "localhost"

    $tmp = $files['tmp_name'];
    $size = $files['size'];
    $type = $files['type'];
    $getExt = explode('/', $type);
    $ext = $getExt[1];
    $name = $nik . '_berkas_' . $kdFile . '.' . $ext;

    $arrType = array("image/*", "application/pdf");
    $allowedType = in_array($type, $arrType);
    $allowedSize = $size < 500000;

    if ($allowedType && $allowedSize) {
        // Initialize upload directory
        $dirUpload = "berkas/" . $nik . "/";
        $checkDir = is_dir($dirUpload);
        if ($checkDir == false) {
            mkdir($dirUpload);
            $upload = move_uploaded_file($tmp, $dirUpload . $name);
        } else {
            $upload = move_uploaded_file($tmp, $dirUpload . $name);
        }
    } else {
        if (!$allowedType) {
            echo 'Jenis file salah';
        }
        if (!$allowedSize) {
            echo "Ukuran salah";
        }
        if (!$allowedType && !$allowedSize) {
            echo "problem occured";
        }
    }

    if ($upload) {
        // Defining file path
        // $path_file = '../../../api_penelitian/' . $dirUpload . $name;
        $path_file = $base . '/' . $dirUpload . $name;

        // Insert file name and file path to the database
        $query = "INSERT INTO peneliti_file (
                  nama_file,
                  path_file,
                  peneliti_id,
                  kd_file
                )
                VALUES (
                    '$name',
                    '$path_file',
                    '$nik',
                    '$kdFile'
                )";

        $stmt = $mypdo->prepare($query);
        $result = $stmt->execute();
    }

    if ($upload && $result) {
        $response = array(
            'status' => 201,
            'message' => 'File has been uploaded.',
            'kode_file' => $kdFile
        );
    } else {
        $response = array(
            'status' => 204,
            'message' => 'Failed to upload file, unknown reason.'
        );
    }

    Flight::json($response);
});

// Get file list per id peneliti
Flight::route('GET /peneliti/@nik/berkas/', function ($nik) use ($mypdo) {
    $query = "SELECT master.*, berkas.*
            FROM peneliti_file_master master
            LEFT JOIN (SELECT a.* FROM peneliti_file a
              WHERE a.`peneliti_id` = $nik) AS berkas
            ON master.kd_file = berkas.kd_file
            WHERE status_file = 'Aktif'";

    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    if (!$result) {
        $response = array(
            'status' => 404,
            'message' => 'No Data Available',
        );
    }

    Flight::json($response);
});

// Delete file
Flight::route('DELETE /peneliti/berkas/@id', function ($id) use ($mypdo) {
    // Getting the file path
    // $selectQuery = "SELECT nama_file, peneliti_id FROM peneliti_file WHERE id=$id";
    // $foo = $mypdo->prepare($selectQuery);
    // $foo->execute();
    // $data = $foo->fetchAll(PDO::FETCH_ASSOC);

    // foreach ($data as $file) {
    //   $filePath = 'berkas/' . $file['peneliti_id'] . '/' . $file['nama_file'];
    // }
    // Delete the file
    // $destroyFile = unlink($filePath);

    $query = "DELETE FROM peneliti_file WHERE id=$id";
    $stmt = $mypdo->prepare($query);
    $stmt->execute();

    $response = array(
        'status' => 200,
        'type' => 'file',
        'message' => 'File has been deleted.'
    );


    Flight::json($response);
});

// Get data master berkas
Flight::route('GET /master_berkas', function () use ($mypdo) {
    $query = "SELECT kd_file, jenis_file, kategori_file, status_file
    FROM peneliti_file_master";

    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    if (!$result) {
        $response = array(
            'status' => 404,
            'message' => 'No Data Available'
        );
    }

    Flight::json($response);
});

Flight::route('POST /master_berkas', function () use ($mypdo) {
    $request = Flight::request()->data;
    $namaBerkas = $request['nama_berkas'];
    $kategori = $request['kategori'];
    $status = $request['status_berkas'];

    $query = "INSERT INTO peneliti_file_master (
      jenis_file, kategori_file, status_file
    ) VALUES (
      '$namaBerkas', '$kategori', '$status'
    )";

    $stmt = $mypdo->prepare($query);
    $result = $stmt->execute();

    if ($result) {
        $response = array(
            'status' => 201,
            'message' => 'Data has been added'
        );
    } else {
        $response = array(
            'status' => 204,
            'message' => 'Failed to save data'
        );
    }

    Flight::json($response);
});

Flight::route('GET /master_berkas/@id', function ($id) use ($mypdo) {
    $query = "SELECT * FROM peneliti_file_master WHERE kd_file = '$id'";
    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    if (!$result) {
        $response = array(
            'status' => 404,
            'message' => 'No data available'
        );
    }

    Flight::json($response);
});

Flight::route('PUT /master_berkas/@id', function ($id) use ($mypdo) {
    $request = json_decode(Flight::request()->getBody());
    $nama = $request->nama;
    $kategori = $request->kategori;
    $status = $request->status;

    $query = "UPDATE peneliti_file_master
    SET jenis_file = '$nama', kategori_file = '$kategori',
    status_file='$status' WHERE kd_file = '$id'";

    $stmt = $mypdo->prepare($query);
    $result = $stmt->execute();

    if ($result) {
        $response = array(
            'status' => 200,
            'message' => 'Data has been updated'
        );
    } else {
        $response = array(
            'status' => 204,
            'message' => 'Failed to update data'
        );
    }

    Flight::json($response);
});

Flight::route('DELETE /master_berkas/@id', function ($id) use ($mypdo) {
    $query = "DELETE FROM peneliti_file_master WHERE kd_file = '$id'";

    $stmt = $mypdo->prepare($query);
    $result = $stmt->execute();

    if ($result) {
        $response = array(
            'status' => 200,
            'message' => 'Data has been deleted'
        );
    } else {
        $response = array(
            'status' => 204,
            'message' => 'Failed to delete the data'
        );
    }

    Flight::json($response);
});

// ! ========================== End Route File ================================



// ! ==========================================================================
// ! Route Untuk Penjadwalan Peneliti
// ! ==========================================================================

/* Get All Data */
Flight::route('GET /penelitian/penjadwalan', function () use ($mypdo) {
    $query = "SELECT j.jadwal_id, p.nama_peneliti, j.jenis_penelitian as jenis,
            j.kategori, j.ruangan, j.tgl_mulai, j.tgl_selesai
        FROM peneliti_jadwal j 
            LEFT JOIN peneliti p ON j.peneliti_id = p.id
        WHERE j.deleted_at IS NULL
        ORDER BY j.jadwal_id DESC";

    $execute = $mypdo->query($query);
    $result = $execute->fetchAll(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    if (!$result) {
        $response = array(
            'status' => 404,
            'message' => 'No Data Available',
        );
    }

    Flight::json($response);
});

/* Get Data by Id */
Flight::route('GET /penelitian/penjadwalan/@id', function ($id) use ($mypdo) {
    $query = "SELECT pj.jadwal_id, pj.peneliti_id, p.nama_peneliti, p.nik, pj.jenis_penelitian,
            pj.kategori, pj.ruangan_id, pj.ruangan, pj.tgl_mulai, pj.tgl_selesai,
            pjb.id AS pjb_id, pjb.pembimbing_id, pjb.pembimbing_nip,
            pjb.pembimbing_nama
        FROM peneliti_jadwal pj
            INNER JOIN peneliti p ON p.id = pj.peneliti_id
            INNER JOIN peneliti_jadwal_pembimbing pjb ON pjb.jadwal_id = pj.jadwal_id
        WHERE pjb.deleted_at IS NULL AND pj.jadwal_id = $id";

    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    if (!$result) {
        $response = array(
            'status' => 404,
            'message' => 'No Data Available',
        );
    }

    Flight::json($response);
});

/* Store Penjadwalan Data */
Flight::route('POST /penelitian/penjadwalan', function () use ($mypdo) {
    $data = Flight::request()->data;
    $dateTime = date('Y-m-d H:i:s');

    try {
        $mypdo->beginTransaction();

        $query = "INSERT INTO peneliti_jadwal (
            peneliti_id,
            jenis_penelitian,
            kategori,
            ruangan_id,
            ruangan,
            tgl_mulai,
            tgl_selesai,
            created_at,
            created_by
          )
          VALUES (
            '$data->peneliti_id',
            '$data->jenis',
            '$data->kategori',
            '$data->ruangan_id',
            '$data->ruangan',
            '$data->tgl_mulai',
            '$data->tgl_selesai',
            '$dateTime',
            '$data->user_id'
          )";

        $mypdo->exec($query);

        $latest = $mypdo->lastInsertId();

        foreach ($data['pembimbing'] as $pembimbing) {
            $insertPembimbing = "INSERT INTO peneliti_jadwal_pembimbing (
                jadwal_id, pembimbing_id, pembimbing_nip, pembimbing_nama, created_at, created_by
            ) VALUES (
                '$latest', '$pembimbing[pembimbing_id]', '$pembimbing[pembimbing_nip]',
                '$pembimbing[pembimbing_nama]', '$dateTime', '$data[user_id]'
            )";

            $mypdo->exec($insertPembimbing);
        }

        $result = $mypdo->commit();

        if ($result) {
            $response = array(
                'status' => 201,
                'message' => 'Data berhasil disimpan.'
            );
        }
    } catch (Exception $e) {
        $mypdo->rollback();

        $response = array(
            'status' => 204,
            'message' => $e->getMessage(),
        );
    }


    Flight::json($response);
});

Flight::route('PUT /penelitian/penjadwalan/@id', function ($id) use ($mypdo) {

    $dateTime = date('Y-m-d H:i:s');
    $request = json_decode(Flight::request()->getBody(), true);

    try {
        $mypdo->beginTransaction();

        $updateJadwal = "UPDATE peneliti_jadwal pj 
                SET pj.jenis_penelitian = '$request[jenis]',
                    pj.kategori = '$request[kategori]',
                    pj.ruangan_id = '$request[ruangan_id]',
                    pj.ruangan = '$request[ruangan]',
                    pj.tgl_mulai = '$request[tgl_mulai]',
                    pj.tgl_selesai = '$request[tgl_selesai]',
                    pj.updated_at = '$dateTime',
                    pj.updated_by = '$request[updated_by]'
            WHERE jadwal_id = $id";

        $execute = $mypdo->exec($updateJadwal);

        if ($execute) {
            foreach ($request['pembimbing'] as $pembimbing) {
                if ($pembimbing['status'] == 'hapus') {
                    $deletePembimbing = "UPDATE peneliti_jadwal_pembimbing
                            SET deleted_at = '$dateTime',
                                deleted_by = '$request[user_id]' 
                        WHERE jadwal_id = '$id' AND id = '$pembimbing[trans_id]'";

                    $mypdo->exec($deletePembimbing);
                } else if ($pembimbing['status'] == 'baru') {
                    $insertPengawas = "INSERT INTO peneliti_jadwal_pembimbing (
                        jadwal_id, 
                        pembimbing_id,
                        pembimbing_nip,
                        pembimbing_nama, 
                        created_at, 
                        created_by 
                    ) VALUES (
                        '$id',
                        '$pembimbing[pembimbing_id]',
                        '$pembimbing[pembimbing_nip]',
                        '$pembimbing[pembimbing_nama]',
                        '$dateTime',
                        '$request[user_id]'
                    )";

                    $mypdo->exec($insertPengawas);
                }
            }
        }
        $result = $mypdo->commit();

        if ($result) {
            $response = array(
                'status' => 201,
                'message' => 'Data berhasil disimpan.',
            );
        }
    } catch (Exception $e) {
        $mypdo->rollback();

        $response = array(
            'status' => 204,
            'message' => $e->getMessage(),
        );
    }

    Flight::json($response);
});

/* Delete Penjadwalan Data */
Flight::route('DELETE /penelitian/penjadwalan/@id', function ($id) use ($mypdo) {
    $request = Flight::request()->data;
    $dateTime = date('Y-m-d H:i:s');

    $query = "UPDATE peneliti_jadwal
            SET deleted_at = '$dateTime',
                deleted_by = '$request->deleted_by'
        WHERE jadwal_id = $id";

    $result = $mypdo->exec($query);

    if (!$result) {
        $response = array(
            'status' => 400,
            'message' => 'Gagal menghapus data.'
        );
    } else {
        $response = array(
            'status' => 200,
            'message' => 'Data berhasil dihapus.'
        );
    }

    Flight::json($response);
});

/** 
 * Get peneliti data who active for scheduling purpose
 * if the peneliti(s) are already scheduled
 * then they are not showed
 */
Flight::route('GET /penelitian/search_peneliti', function () use ($mypdo) {
    $nama = Flight::request()->query->nama;

    $query = "SELECT id, nik, nama_peneliti FROM peneliti 
        WHERE status_peneliti = 1
            AND deleted_at IS NULL
            AND nama_peneliti LIKE '%$nama%'";

    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    if (!$result) {
        $response = array(
            'status' => 404,
            'message' => 'No Data Available',
        );
    }

    Flight::json($response);
});

/* Get Pembimbing */
Flight::route('GET /pembimbing', function () use ($mypdo) {
    $query = "SELECT dosen_id, pegawai_id, non_nik, non_nama FROM dosen WHERE pembimbing = 1";

    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    if (!$result) {
        $response = array(
            'status' => 404,
            'message' => 'No Data Available'
        );
    }

    Flight::json($response);
});



/* Store Penjadwalan Data */
Flight::route('POST /penelitian/tarif', function () use ($mypdo) {
    $data = Flight::request()->data;
    $dateTime = date('Y-m-d H:i:s');

    try {
        $mypdo->beginTransaction();

        //        $query = "INSERT INTO peneliti_tarif (
        //            jenis_id, pendidikan_id, biaya
        //          )
        //          VALUES (
        //              $data->jenis_
        //          )";

        //        $mypdo->exec($query);

        $latest = $mypdo->lastInsertId();

        foreach ($data['pembimbing'] as $pembimbing) {
            $insertPembimbing = "INSERT INTO peneliti_jadwal_pembimbing (
                jadwal_id, pembimbing_id, pembimbing_nip, pembimbing_nama, created_at, created_by
            ) VALUES (
                '$latest', '$pembimbing[pembimbing_id]', '$pembimbing[pembimbing_nip]',
                '$pembimbing[pembimbing_nama]', '$dateTime', '$data[user_id]'
            )";

            $mypdo->exec($insertPembimbing);
        }

        $result = $mypdo->commit();

        if ($result) {
            $response = array(
                'status' => 201,
                'message' => 'Data berhasil disimpan.'
            );
        }
    } catch (Exception $e) {
        $mypdo->rollback();

        $response = array(
            'status' => 204,
            'message' => $e->getMessage(),
        );
    }


    Flight::json($response);
});

// ! =========================== End Penjadwalan ==============================



// ! ==========================================================================
// ! Route untuk cek ketersediaan NIK
// ! ==========================================================================
Flight::route('GET /peneliti/nik/@nik', function ($nik) use ($mypdo) {
    $query = "SELECT id, nik FROM peneliti WHERE nik = $nik
              AND delete_at = '0000-00-00 00:00:00'";
    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 'used',
        'message' => 'NIK sudah digunakan'

    );

    if (!$result) {
        $response = array(
            'status' => 'available',
            'message' => 'NIK belum digunakan'
        );
    }

    Flight::json($response);
});



// ! ==========================================================================
// ! Route untuk keuangan
// ! ==========================================================================
Flight::route('GET /keuangan/jenis_bayar', function () use ($mypdo) {
    // $query = "SELECT * FROM ku_jenisbayar WHERE byr_aktif = 1";
    // $stmt = $mypdo->prepare($query);
    // $stmt->execute();
    // $result = $stmt->fetchAll(PDO::FETCH_ASSOC);


    // Flight::json($response)
});


// ! =========================== End Keuangan =================================

// ! ==========================================================================
// ! Route untuk nilai DM
// ! ==========================================================================
Flight::route('GET /dm/nilai/@id/@tgl', function ($id, $tgl) {
    $query = "SELECT
      penilaian.`penilaian_id`,
      penilaian.`penilaian_tgl`,
      nilai_jenis.`jns_nilai`,
      ms_smf.`smf_nama`,
      penilaian.`kasus`,
      peserta_klinik.`pk_hasil`,
      peserta_klinik.`pk_nilai_akhir`,
      peserta_klinik.`pk_nilai_huruf`,
      siswa.`siswa_npm`,
      siswa.`siswa_nama`
    FROM
      penilaian
      INNER JOIN peserta_klinik
        ON peserta_klinik.`smf_id` = penilaian.`smf_id`
      INNER JOIN nilai_jenis
        ON nilai_jenis.`jnsnilai_id` = penilaian.`jnsnilai_id`
      INNER JOIN ms_smf
        ON ms_smf.`smf_id` = penilaian.`smf_id`
      LEFT JOIN peserta_didik
        ON peserta_didik.`pd_id` = peserta_klinik.`pd_id`
      INNER JOIN siswa
        ON siswa.`siswa_id` = peserta_didik.`siswa_id`
    WHERE siswa.siswa_id = '$id'
      AND penilaian.`penilaian_tgl` = '$tgl'";

    $pendidikanConn = connectDbPendidikan();
    $stmt = $pendidikanConn->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if ($result) {
        $response = array(
            'status' => 200,
            'message' => 'Success',
            'data' => $result
        );
    } else {
        $response = array(
            'status' => 404,
            'message' => 'No Data Available'
        );
    }

    Flight::json($response);
});

// ! ========================= End nilai DM ===================================

Flight::route('GET /master/ksm', function () {
    $connection = connectDbPendidikan();
    $query = "SELECT * FROM ms_smf";

    $stmt = $connection->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});

Flight::route('GET /master/universitas', function () {
    $connection = connectDbPendidikan();

    $query = "SELECT * FROM ms_instansi";
    $stmt = $connection->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});


// ! ==========================================================================
// ! Route untuk laporan nilai
// ! ==========================================================================
Flight::route('GET /laporan/nilai/@idPenilaian', function ($idPenilaian) use ($mypdo) {
    $query = "SELECT
              pk.`pk_id`,
              pk.`pk_mulai`,
              pk.`pk_selesai`,
              s.`siswa_id`,
              s.`siswa_nama`,
              mi.`instansi_nama`,
              ms.`smf_nama`,
              mj.`jenis`,
              pk.`pk_semester`,
              d.`non_nama`,
              d.`pegawai_id`,
              IF(
                pp.`status` = 1,
                'Penguji',
                'Pembimbing'
              ) tipe,
              nm.`nm_bobot`,
              msmf.`mtr_id`,
              msmf.`mtr_ket`,
              nm.`nm_nilai`,
              pk.`pk_hasil`,
              pk.`pk_nilai_akhir`,
              pk.`pk_nilai_rata`
            FROM
              peserta_klinik pk
              JOIN peserta_didik pd
                ON pd.`pd_id` = pk.`pd_id`
              JOIN nilai_materi nm
                ON pk.`pk_id` = nm.`pk_id`
              JOIN materi_smf msmf
                ON nm.`mtr_id` = msmf.`mtr_id`
              JOIN siswa s
                ON s.`siswa_id` = pd.`siswa_id`
              JOIN ms_instansi mi
                ON s.`instansi_id` = mi.`instansi_id`
              JOIN ms_smf ms
                ON pk.`smf_id` = ms.`smf_id`
              JOIN ms_jnskel mj
                ON pd.`pd_jenis` = mj.`kd`
              JOIN dosen d
                ON pk.`smf_id` = d.`smf_id`
              JOIN penilaian_penguji pp
                ON d.`dosen_id` = pp.`dosen_id`
            WHERE nm.`penilaian_id` = '$idPenilaian'
            AND pp.`penilaian_id` = '$idPenilaian'
            GROUP BY siswa_id, mtr_id
            ORDER BY siswa_nama ASC";

    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $dataArr = array();
    $id = '';

    foreach ($result as $dataNilai) {
        if ($id != $dataNilai['siswa_id']) {
            $id = $dataNilai['siswa_id'];

            $dataArr[$dataNilai['siswa_id']] = array(
                'tgl_mulai' => $dataNilai['pk_mulai'],
                'tgl_selesai' => $dataNilai['pk_selesai'],
                'nama_siswa' => $dataNilai['siswa_nama'],
                'institusi' => $dataNilai['instansi_nama'],
                'smf' => $dataNilai['smf_nama'],
                'stase' => $dataNilai['jenis'],
                'semester' => $dataNilai['pk_semester'],
                'pendidik' => $dataNilai['non_nama'],
                'tipe_pendidik' => $dataNilai['tipe'],
                'pegawai_id' => $dataNilai['pegawai_id'],
                'nilai_akhir' => $dataNilai['pk_hasil'],
                'nilai_ujian' => $dataNilai['pk_nilai_akhir'],
                'nilai_rata' => $dataNilai['pk_nilai_rata'],
                'nilai_materi' => array()
            );
        }

        $isiNilai = array(
            'nama_materi' => $dataNilai['mtr_ket'],
            'nilai_materi' => $dataNilai['nm_nilai']
        );
        // $dataArr[$id]['nilai_materi'][$dataNilai['mtr_ket']] = $dataNilai['nm_nilai'];
        array_push($dataArr[$id]['nilai_materi'], $isiNilai);
    }


    // response
    if ($result) {
        $response = array(
            'status' => 200,
            'message' => 'success',
            'data' => array()
        );

        foreach ($dataArr as $arr) {
            array_push($response['data'], $arr);
        }
    } else {
        $response = array(
            'status' => 404,
            'message' => 'Data not found.'
        );
    }

    Flight::json($response);
});
// ! ==========================================================================


// ! ==========================================================================
// ! Route untuk pembayaran penelitian
// ! ==========================================================================

// Show all pembayaran 
Flight::route('GET /pembayaran/penelitian', function () use ($mypdo) {
    $query = "SELECT
              pp.*,
              DATE_FORMAT(pp.`created_at`, '%d-%m-%Y') as tgl_pembayaran,
              p.`nik`,
              p.`nama_peneliti`,
              if (
                pj.`jenis_penelitian` = 1,
                'Data Awal',
                'Penelitian'
              ) as jenis,
              DATE_FORMAT(pj.`tgl_mulai`, '%d-%m-%Y') AS periode
            FROM
              peneliti_pembayaran pp
              JOIN peneliti p
                ON pp.`peneliti_id` = p.`id`
              JOIN peneliti_jadwal pj
                ON pj.`peneliti_id` = p.`id`
              ORDER BY pp.`id` DESC";

    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    if (!$result) {
        $response = array(
            'status' => 404,
            'message' => 'No Data Available',
        );
    }

    Flight::json($response);
});

// Insert pembayaran penelitian
Flight::route('POST /pembayaran/penelitian', function () use ($mypdo) {
    $request      = Flight::request()->data;
    $penelitiId   = $request->nama;
    $totalBayar   = $request->jml_tagihan;
    $noKwitansi   = $request->no_kwitansi;
    $tglKwitansi  = $request->tgl_kwitansi;
    $durasi       = $request->durasi;
    $jumlahRuang  = $request->jml_ruangan;
    $keterangan   = $request->keterangan;
    $today        = date('Y-m-d H:i:s');

    if (
        empty($penelitiId) or
        empty($noKwitansi) or
        empty($durasi)
    ) {
        $response = array(
            'status' => 403,
            'message' => 'Failed, your data is not complete.'
        );

        Flight::json($response);
        die();
    }

    $query = "INSERT INTO peneliti_pembayaran 
            (
              peneliti_id,
              no_kwitansi,
              tgl_kwitansi,
              jumlah_bayar,
              durasi,
              jml_ruangan,
              keterangan,
              created_at
            ) VALUES 
            (
              '$penelitiId',
              '$noKwitansi',
              '$tglKwitansi',
              '$totalBayar',
              '$durasi',
              '$jumlahRuang',
              '$keterangan',
              '$today'
            )";

    $stmt = $mypdo->prepare($query);
    $result = $stmt->execute();

    if ($result) {
        $response = array(
            'status' => 201,
            'message' => 'Data has been added.'
        );
    } else {
        $response = array(
            'status' => 204,
            'message' => 'Failed to save the data.'
        );
    }

    Flight::json($response);
});

Flight::route('PUT /pembayaran/penelitian/@id', function ($id) use ($mypdo) {
    $request = json_decode(Flight::request()->getBody());

    $nama       = $request->nama;
    $ruang      = $request->jml_ruangan;
    $durasi     = $request->durasi;
    $bayar      = $request->jml_tagihan;
    $kwitansi   = $request->no_kwitansi;
    $tglKw      = $request->tgl_kwitansi;
    $keterangan = $request->keterangan;
    $tglUpdate  = date('Y-m-d H:i:s');


    $query    = "UPDATE
                  peneliti_pembayaran
               SET
                  peneliti_id = '$nama',
                  jml_ruangan = '$ruang',
                  durasi = '$durasi',
                  jumlah_bayar = '$bayar',
                  no_kwitansi = '$kwitansi',
                  tgl_kwitansi = '$tglKw',
                  keterangan = '$keterangan',
                  updated_at = '$tglUpdate'
               WHERE
                  id = '$id'";

    $stmt = $mypdo->prepare($query);
    $result = $stmt->execute();

    if ($result) {
        $response = array(
            'status' => 200,
            'message' => 'Data has been updated.'
        );
    } else {
        $response = array(
            'status' => 204,
            'message' => 'Failed to update the data.'
        );
    }

    Flight::json($response);
});

Flight::route('DELETE /pembayaran/penelitian/@id', function ($id) use ($mypdo) {
    $query = "DELETE FROM peneliti_pembayaran WHERE id='$id'";

    $stmt = $mypdo->prepare($query);
    $result = $stmt->execute();

    if ($result) {
        $response = array(
            'status' => 200,
            'message' => 'Data has been deleted.'
        );
    }

    Flight::json($response);
});

// Get peneliti data for dropdown
Flight::route('GET /pembayaran/penelitian/get_peneliti', function () use ($mypdo) {
    $query = "SELECT p.`id`, p.`nik`, p.`nama_peneliti`
            FROM peneliti p 
            LEFT JOIN peneliti_jadwal j 
              ON p.`id` = j.`peneliti_id`
            WHERE p.`status_peneliti` = '1'";

    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    if (!$result) {
        $response = array(
            'status' => 404,
            'message' => 'No Data Available',
        );
    }

    Flight::json($response);
});

Flight::route('GET /pembayaran/penelitian/get_peneliti/@id', function ($id) use ($mypdo) {
    $query = "SELECT p.*, 
              j.`ruangan`,
              j.`jenis_penelitian`,
              IF (
                    j.`jenis_penelitian` = 1, 
                    'Data Awal',
                    'Penelitian'
                  ) AS nama_penelitian
            FROM peneliti p
            LEFT JOIN peneliti_jadwal j 
              ON p.`id` = j.`peneliti_id` 
            WHERE p.`id` = $id";

    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    if (!$result) {
        $response = array(
            'status' => 404,
            'message' => 'No Data Available',
        );
    }

    Flight::json($response);
});

Flight::route('GET /pembayaran/penelitian/@id', function ($id) use ($mypdo) {
    $query = "SELECT
              pp.`id`,
              pp.`peneliti_id`,
              p.`nama_peneliti`,
              pp.`no_kwitansi`,
              pp.`tgl_kwitansi`,
              pp.`jumlah_bayar`,
              pp.`durasi`,
              pp.`jml_ruangan`,
              p.`instansi`,
              pj.`jenis_penelitian`,
              IF (
                    pj.`jenis_penelitian` = 1, 
                    'Data Awal',
                    'Penelitian'
                  ) AS nama_penelitian,
              pj.`ruangan`,
              pp.`jumlah_bayar`,
              pp.`verif`,
              pp.`keterangan`
            FROM
              peneliti_pembayaran pp
              LEFT JOIN peneliti p
                ON pp.`peneliti_id` = p.`id`
              LEFT JOIN peneliti_jadwal pj
                ON p.`id` = pj.`peneliti_id`
            WHERE pp.`id` = '$id'";
    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($result) {
        $response = array(
            'status' => 200,
            'message' => 'success',
            'data' => $result
        );
    }

    Flight::json($response);
});

Flight::route('GET /pembayaran/biaya/@name', function ($name) use ($mypdo) {
    $query = "SELECT * FROM ku_jenisbayar WHERE byrnama='$name'";

    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($result) {
        $response = array(
            'status' => 200,
            'message' => 'success',
            'data' => $result
        );
    } else {
        $response = array(
            'status' => 404,
            'message' => 'Data not found.'
        );
    }

    Flight::json($response);
});

Flight::route('GET /pembayaran/jenis_bayar_penelitian', function () use ($mypdo) {

    $query = "SELECT byrjns_id, byrjns, byrnama, byr_nilai
              FROM ku_jenisbayar 
            WHERE byrjns LIKE 'penelitian%' AND byr_aktif = '1'";

    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if ($result) {
        $response = array(
            'status' => 200,
            'message' => 'success',
            'data' => $result
        );
    } else {
        $response = array(
            'status' => 404,
            'message' => 'Data not found.'
        );
    }

    Flight::json($response);
});

// ! ==========================================================================

// ! ==========================================================================
// ! Route untuk tagihan penelitian
// ! ==========================================================================

Flight::route('GET /penelitian/tagihan/peneliti', function () use ($mypdo) {
    $nama = Flight::request()->query->nama;
    $query = "SELECT p.id AS peneliti_id, p.nik AS peneliti_nik, p.nama_peneliti AS peneliti_nama,
            p.instansi, pj.jadwal_id, pj.jenis_penelitian, pj.tgl_mulai, pj.tgl_selesai
        FROM peneliti p
            INNER JOIN peneliti_jadwal pj ON pj.peneliti_id = p.id 
        WHERE pj.deleted_at IS NULL
            AND p.nama_peneliti like '%$nama%'
        GROUP BY p.id";

    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    if (!$result) {
        $response = array(
            'status' => 404,
            'message' => 'No Data Available',
        );
    }

    Flight::json($response);
});

Flight::route('GET /penelitian/tagihan/jenis/@peneliti', function ($peneliti) use ($mypdo) {
    $query = "SELECT pj.jadwal_id, pj.jenis_penelitian, pj.ruangan_id, pj.ruangan,
            pj.tgl_mulai, pj.tgl_selesai
        FROM peneliti_jadwal pj WHERE pj.peneliti_id = $peneliti";

    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    if (!$result) {
        $response = array(
            'status' => 404,
            'message' => 'No Data Available',
        );
    }

    Flight::json($response);
});

// ! ==========================================================================



// ! ==========================================================================
// ! Route untuk Laporan Tagihan dan Pembayaran
// ! ==========================================================================

Flight::route('GET /laporan/tagihan/penelitian', function () use ($mypdo) {
    $query = "SELECT
              DATE_FORMAT(pp.`tgl_kwitansi`, '%d-%m-%Y') AS tgl_kwitansi,
              p.`instansi`,
              pp.`no_kwitansi`,
              p.`nama_peneliti`,
              DATE_FORMAT(pj.`tgl_mulai`, '%d-%m-%Y') AS tgl_mulai,
              DATE_FORMAT(pj.`tgl_selesai`, '%d-%m-%Y') AS tgl_selesai,
              pp.`durasi`,
              pp.`jml_ruangan` AS ruang,
              REPLACE(FORMAT(pp.`jumlah_bayar`, 0), ',', '.') AS total,
              REPLACE(FORMAT(
                pp.`jumlah_bayar`/pp.`jml_ruangan`/pp.`durasi`, 0
              ), ',', '.') AS rincian,
              IF (
                pj.`jenis_penelitian` = 1,
                'Data Awal',
                'Penelitian'
              ) AS jenis
            FROM
              peneliti_pembayaran pp
              JOIN peneliti p
                ON pp.`peneliti_id` = p.`id`
              JOIN peneliti_jadwal pj
                ON pj.`peneliti_id` = p.`id`
            ORDER BY pp.`id` DESC";

    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $qTotal = "SELECT
                REPLACE(FORMAT(SUM(jumlah_bayar), 0), ',', '.') AS grand_total
              FROM
                peneliti_pembayaran";
    $stmtTotal = $mypdo->prepare($qTotal);
    $stmtTotal->execute();
    $resultTotal = $stmtTotal->fetch(PDO::FETCH_COLUMN);

    $qDataAwal = "SELECT
                  REPLACE(FORMAT(SUM(pp.`jumlah_bayar`),0), ',', '.') AS total_data_awal
                FROM
                  peneliti_pembayaran pp
                  JOIN peneliti p
                    ON pp.`peneliti_id` = p.`id`
                  JOIN peneliti_jadwal j
                    ON p.`id` = j.`peneliti_id`
                WHERE j.`jenis_penelitian` = '1'";

    $stmtDataAwal = $mypdo->prepare($qDataAwal);
    $stmtDataAwal->execute();
    $resultDataAwal = $stmtDataAwal->fetch(PDO::FETCH_COLUMN);

    $qPenelitian = "SELECT
                      REPLACE(FORMAT(SUM(pp.`jumlah_bayar`),0), ',', '.') AS total_penelitian
                    FROM
                      peneliti_pembayaran pp
                      JOIN peneliti p
                        ON pp.`peneliti_id` = p.`id`
                      JOIN peneliti_jadwal j
                        ON p.`id` = j.`peneliti_id`
                    WHERE j.`jenis_penelitian` = '2'";

    $stmtPenelitian = $mypdo->prepare($qPenelitian);
    $stmtPenelitian->execute();
    $resultPenelitian = $stmtPenelitian->fetch(PDO::FETCH_COLUMN);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'total_pembayaran' => array(
            'pembayaran_data_awal' => $resultDataAwal,
            'pembayaran_penelitian' => $resultPenelitian,
            'grand_total' => $resultTotal
        ),
        'data' => $result
    );

    if (!$result) {
        $response = array(
            'status' => 404,
            'message' => 'No Data Available',
        );
    }

    Flight::json($response);
});

Flight::route('GET /laporan/tagihan/penelitian/@tahun/@bulan', function ($tahun, $bulan) use ($mypdo) {
    $query = "SELECT
              DATE_FORMAT(pp.`tgl_kwitansi`, '%d-%m-%Y') AS tgl_kwitansi,
              pp.`no_kwitansi`,
              p.`instansi`,
              p.`nama_peneliti`,
              DATE_FORMAT(pj.`tgl_mulai`, '%d-%m-%Y') AS tgl_mulai,
              DATE_FORMAT(pj.`tgl_selesai`, '%d-%m-%Y') AS tgl_selesai,
              pp.`durasi`,
              pp.`jml_ruangan` AS ruang,
              REPLACE(FORMAT(pp.`jumlah_bayar`, 0), ',', '.') AS total,
              REPLACE(FORMAT(
                pp.`jumlah_bayar`/pp.`jml_ruangan`/pp.`durasi`, 0
              ), ',', '.') AS rincian,
              IF (
                pj.`jenis_penelitian` = 1,
                'Data Awal',
                'Penelitian'
              ) AS jenis
            FROM
              peneliti_pembayaran pp
              JOIN peneliti p
                ON pp.`peneliti_id` = p.`id`
              JOIN peneliti_jadwal pj
                ON pj.`peneliti_id` = p.`id`
            WHERE pp.`tgl_kwitansi` LIKE '$tahun-$bulan%'
            ORDER BY pp.`id` DESC";

    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $qTotal = "SELECT
                REPLACE(FORMAT(SUM(jumlah_bayar), 0), ',', '.') AS grand_total
              FROM
                peneliti_pembayaran";
    $stmtTotal = $mypdo->prepare($qTotal);
    $stmtTotal->execute();
    $resultTotal = $stmtTotal->fetch(PDO::FETCH_COLUMN);

    $qDataAwal = "SELECT
                  REPLACE(FORMAT(SUM(pp.`jumlah_bayar`),0), ',', '.') AS total_data_awal
                FROM
                  peneliti_pembayaran pp
                  JOIN peneliti p
                    ON pp.`peneliti_id` = p.`id`
                  JOIN peneliti_jadwal j
                    ON p.`id` = j.`peneliti_id`
                WHERE j.`jenis_penelitian` = '1'";

    $stmtDataAwal = $mypdo->prepare($qDataAwal);
    $stmtDataAwal->execute();
    $resultDataAwal = $stmtDataAwal->fetch(PDO::FETCH_COLUMN);

    $qPenelitian = "SELECT
                      REPLACE(FORMAT(SUM(pp.`jumlah_bayar`),0), ',', '.') AS total_penelitian
                    FROM
                      peneliti_pembayaran pp
                      JOIN peneliti p
                        ON pp.`peneliti_id` = p.`id`
                      JOIN peneliti_jadwal j
                        ON p.`id` = j.`peneliti_id`
                    WHERE j.`jenis_penelitian` = '2'";

    $stmtPenelitian = $mypdo->prepare($qPenelitian);
    $stmtPenelitian->execute();
    $resultPenelitian = $stmtPenelitian->fetch(PDO::FETCH_COLUMN);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'total_pembayaran' => array(
            'pembayaran_data_awal' => $resultDataAwal,
            'pembayaran_penelitian' => $resultPenelitian,
            'grand_total' => $resultTotal
        ),
        'data' => $result
    );

    if (!$result) {
        $response = array(
            'status' => 404,
            'message' => 'No Data Available',
        );
    }

    Flight::json($response);
});

Flight::route('GET /laporan/tagihan/keperawatan', function () use ($mypdo) {

    $query = "SELECT
              t.`tagihan_tgl`,
              t.`tagihan_kode`,
              mi.`instansi_nama`,
              t.`periode`,
              t.`periode_selesai`,
              t.`jumlah`,
              mj.`jenis`,
              REPLACE(FORMAT(t.`biaya_praktek`,0), ',','.') AS biaya_praktek,
              REPLACE(FORMAT(t.`biaya_ujian`,0), ',','.') AS biaya_ujian,
              t.`lama` AS lama_bulan,
              ((t.`biaya_praktek` * t.`lama`) + t.`biaya_ujian`) AS compare,
              REPLACE(FORMAT(t.`nilai`,0), ',', '.') AS nilai_tagihan
            FROM
              ku_tagihan t
              JOIN ms_instansi mi
              ON t.`instansi_id` = mi.`instansi_id`
              JOIN ms_jnskel mj
              ON t.`stase` = mj.`id`";

    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if ($result) {
        $response = array(
            'status' => 200,
            'message' => 'success',
            'data' => $result
        );
    } else {
        $response = array(
            'status' => 404,
            'message' => 'Data not found'
        );
    }

    Flight::json($response);
});

Flight::route('GET /laporan/tagihan/keperawatan/@tahun/@bulan', function ($tahun, $bulan) use ($mypdo) {
    $query = "SELECT
              t.`tagihan_tgl`,
              t.`tagihan_kode`,
              mi.`instansi_nama`,
              t.`periode`,
              t.`periode_selesai`,
              t.`jumlah`,
              t.`jumlah_ujian`,
              mj.`jenis`,
              REPLACE(FORMAT(t.`biaya_praktek`,0), ',', '.') AS biaya_praktek,
              REPLACE(FORMAT(t.`biaya_ujian`,0), ',', '.') AS biaya_ujian,
              t.`lama`,
              ((t.`biaya_praktek` * t.`lama`) + t.`biaya_ujian`) AS compare,
              REPLACE(FORMAT(t.`nilai`, 0), ',', '.') AS nilai_tagihan,
              REPLACE(FORMAT(t.`jumlah` * t.`biaya_praktek` * t.`lama`, 0), ',', '.') AS tagihan_praktek,
              REPLACE(FORMAT(t.`jumlah_ujian` * t.`biaya_ujian`, 0), ',', '.') AS tagihan_ujian
            FROM
              ku_tagihan t
              JOIN ms_instansi mi
              ON t.`instansi_id` = mi.`instansi_id`
              JOIN ms_jnskel mj
              ON t.`stase` = mj.`id`
            WHERE t.`tagihan_tgl` LIKE '$tahun-$bulan%'";

    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $queryTotal = "SELECT REPLACE(FORMAT(SUM(nilai),0), ',', '.') FROM ku_tagihan";

    $stmtTotal = $mypdo->prepare($queryTotal);
    $stmtTotal->execute();
    $resultTotal = $stmtTotal->fetch(PDO::FETCH_COLUMN);

    if ($result) {
        $response = array(
            'status' => 200,
            'message' => 'success',
            'total_tagihan' => $resultTotal,
            'data' => $result
        );
    } else {
        $response = array(
            'status' => 404,
            'message' => 'Data not found'
        );
    }

    Flight::json($response);
});
// ! ==========================================================================


Flight::route('GET /master/ksm', function () {
    $pendidikanConn = connectDbPendidikan();
    $query = "SELECT * FROM ms_smf";

    $stmt = $pendidikanConn->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});

Flight::route('GET /master/universitas', function () {
    $pendidikanConn = connectDbPendidikan();
    $query = "SELECT * FROM ms_instansi";
    $stmt = $pendidikanConn->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});



// ! ===================================================================================
// ! Route baru untuk simrs pendidikan baru
// ! ===================================================================================

// Flight::route('GET /master/institusi', function () use ($db) {
//     $stmt = $db->prepare("SELECT * FROM m_institusi ORDER BY nama ASC");

//     $stmt->execute();
//     $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
//     $response = array(
//         'status' => 200,
//         'message' => 'success',
//         'data' => $result
//     );

//     Flight::json($response);
// });

// Flight::route('GET /master/prodi', function () use ($db) {
//     $stmt = $db->prepare("SELECT * FROM m_prodi ORDER BY nama ASC");

//     $stmt->execute();
//     $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

//     $response = array(
//         'status' => 200,
//         'message' => 'success',
//         'data' => $result
//     );

//     Flight::json($response);
// });

Flight::route('GET /master/agama', function () use ($db) {
    $stmt = $db->prepare("SELECT * FROM m_agama");

    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});

Flight::route('GET /master/pegawai', function () {
    $request = Flight::request()->query->nama;
    if ($request) {
        $noSpaceRequest = str_replace(" ", "%20", $request);
        $response = http_request("https://api.rsudsidoarjo.co.id/rest_pegawai/sdm/" . $noSpaceRequest);
        $json = json_decode($response);
        $data = array();
        foreach ($json->mysql_sdm as $pegawai) {
            array_push($data, $pegawai);
        }

        if ($response) {
            $result = array(
                'status' => 200,
                'message' => 'success',
                'data' => $data
            );
        }
    } else {
        $result = array(
            'status' => 404,
            'message' => 'Page not found',
        );
    }

    Flight::json($result);
});

Flight::route('GET /mahasiswa(/jenis/@id)', function ($id) use ($db) {
    if ($id != null && $id != 0) {
        $query = "SELECT mpd.*, mi.nama AS institusi_nama, mp.nama AS prodi_nama, mp.jenjang 
      FROM m_peserta_didik mpd 
        LEFT JOIN m_institusi mi 
          ON mi.id = mpd.institusi_id 
        LEFT JOIN m_prodi mp
          ON mp.id = mpd.prodi_id
      WHERE jenis = $id
      ORDER BY id DESC";
    } else if ($id == 0) {
        $query = "SELECT mpd.*, mi.nama AS institusi_nama, mp.nama AS prodi_nama, mp.jenjang  
      FROM m_peserta_didik mpd 
        LEFT JOIN m_institusi mi 
          ON mi.id = mpd.institusi_id 
        LEFT JOIN m_prodi mp
          ON mp.id = mpd.prodi_id
      ORDER BY id DESC";
    }

    $stmt = $db->prepare($query);

    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});

Flight::route('GET /mahasiswa/@id', function ($id) use ($db) {
    $query = "SELECT * FROM m_peserta_didik WHERE id = $id";

    $stmt = $db->prepare($query);

    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});

Flight::route('POST /mahasiswa', function () use ($db) {
    $request = Flight::request()->data;
    $date = date('Y-m-d H:i:s');

    $query = "INSERT INTO m_peserta_didik (
            nim,
            nik,
            nama,
            tempat_lahir,
            tgl_lahir,
            jenis_kelamin,
            agama,
            no_telepon,
            email,
            alamat,
            institusi_id,
            prodi_id,
            jenis,
            aktif,
            created_at,
            created_by
        )
        VALUES (
            '$request->no_id',
            '$request->nik',
            '$request->nama',
            '$request->tempat_lahir',
            '$request->tgl_lahir',
            '$request->jenis_kelamin',
            '$request->agama',
            '$request->no_telepon',
            '$request->email',
            '$request->alamat',
            '$request->institusi',
            '$request->prodi',
            '$request->jenis',
            '$request->aktif',
            '$date',
            '$request->user_id'
        )";

    $stmt = $db->prepare($query);
    $result = $stmt->execute();

    if ($result) {
        $response = array(
            'status' => 201,
            'message' => 'Data berhasil disimpan.',
        );
    } else {
        $response = array(
            'status' => 400,
            'message' => 'Gagal menyimpan data.',

        );
    }

    Flight::json($response);
});

Flight::route('PUT /mahasiswa/@id', function ($id) use ($db) {
    $request = json_decode(Flight::request()->getBody());

    $query = "UPDATE m_peserta_didik 
    SET 
      nim = '$request->nim',
      nik = '$request->nik',
      nama = '$request->nama',
      tempat_lahir = '$request->tempat_lahir',
      tgl_lahir = '$request->tgl_lahir',
      jenis_kelamin = '$request->jenis_kelamin',
      agama = '$request->agama',
      no_telepon = '$request->no_telepon',
      email = '$request->email',
      alamat = '$request->alamat',
      institusi_id = '$request->institusi',
      prodi_id = '$request->prodi',
      jenis = '$request->jenis',
      aktif = '$request->aktif',
      updated_at = date('Y-m-d H:i:s'),
      updated_by = 1
    WHERE id = $id";

    $stmt = $db->prepare($query);
    $result = $stmt->execute();


    if ($result) {
        $response = array(
            'status' => 201,
            'message' => 'Berhasil mengupdate data.'
        );
    } else {
        $response = array(
            'status' => 204,
            'message' => 'Gagal mengupdate data.'
        );
    }


    Flight::json($response);
});

Flight::route('GET /penjadwalan_tes/cari/mahasiswa/@group/@jenis/@institusi/@prodi/@departemen', function ($group, $jenis, $institusi, $prodi, $departemen) use ($db) {
    if ($group == "0") {
        $query = "SELECT id, nama, nim, nik FROM m_peserta_didik 
                  WHERE institusi_id = $institusi 
                    AND aktif = 1 
                    AND prodi_id = $prodi 
                    AND id NOT IN (SELECT peserta_didik_id FROM t_jadwal_tes WHERE jenis_tes = $jenis AND departemen_id = $departemen)";
    } else {
        $query = "SELECT id, nama, nim, nik FROM m_peserta_didik 
                  WHERE institusi_id = $institusi 
                    AND jenis = $group
                    AND aktif = 1 
                    AND prodi_id = $prodi 
                    AND id NOT IN (SELECT peserta_didik_id FROM t_jadwal_tes WHERE jenis_tes = $jenis AND departemen_id = $departemen)";
    }
    // var_dump($query); die;
    $stmt = $db->prepare($query);

    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});

Flight::route('GET /penjadwalan_tes', function () use ($db) {
    $id = Flight::request()->query['group'];
    $query = "SELECT 
                j.id, 
                j.tanggal_tes, 
                pd.nim, 
                j.nama_peserta, 
                j.jenis_tes,
                i.nama AS institusi,
                d.nama AS departemen, 
                pr.nama AS prodi, 
                pd.jenis AS jenis_pendidikan 
              FROM t_jadwal_tes j 
                LEFT JOIN m_institusi i ON j.`institusi_id`=i.`id`
                LEFT JOIN m_peserta_didik pd ON pd.id=j.`peserta_didik_id`
                LEFT JOIN m_prodi pr ON j.`program_studi_id`=pr.`id`
                LEFT JOIN m_departemen d ON j.`departemen_id`=d.`id`";

    if ($id != 0) {
        $query .= " WHERE pd.jenis = '$id' ";
    }

    $stmt = $db->prepare($query);

    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});

Flight::route('GET /penjadwalan_tes/@id', function ($id) use ($db) {
    $query = "SELECT * FROM t_jadwal_tes t 
                INNER JOIN t_jadwal_tes_pengawas p ON p.jadwal_tes_id = t.id
                    AND p.deleted_at IS NULL 
                WHERE t.id='{$id}'";

    $stmt = $db->prepare($query);
    $stmt->execute();

    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});

Flight::route('POST /penjadwalan_tes', function () use ($db) {
    $request = Flight::request()->data;
    $date = date('Y-m-d');
    $dateTime = date('Y-m-d H:i:s');

    try {
        $db->beginTransaction();

        $arrPengawas = array();
        foreach ($request['pengawas'] as $pegawai) {
            array_push($arrPengawas, array(
                "pengawas_id" => $pegawai['pengawas_id'],
                "pengawas_nip" => $pegawai['pengawas_nip'],
                "pengawas_nama" => $pegawai['pengawas_nama'],
            ));
        }

        foreach ($request['peserta_didik'] as $peserta) {
            $query = "INSERT INTO t_jadwal_tes (         
                tanggal_tes,
                peserta_didik_id,
                nama_peserta,
                jenis_tes,
                institusi_id,
                departemen_id,
                program_studi_id,
                tanggal,
                tanggal_act,
                user_id,
                status
            )
            VALUES (
                '$peserta[tgl_tes]',
                '$peserta[peserta_id]',
                '$peserta[nama_peserta]',
                '$peserta[jenis_id]',
                '$peserta[institusi_id]',
                '$peserta[departemen_id]',
                '$peserta[program_studi_id]',
                '$date',
                '$dateTime',
                '$peserta[user_id]',
                1
            )";

            $stmt = $db->prepare($query);
            $stmt->execute();

            $insertedId = $db->lastInsertId();

            foreach ($arrPengawas as $pengawas) {
                $query2 = "INSERT INTO t_jadwal_tes_pengawas (
                          jadwal_tes_id, 
                          pengawas_id,
                          pengawas_nip,
                          pengawas_nama, 
                          created_at, 
                          created_by 
                      ) VALUES (
                          '$insertedId',
                          '$pengawas[pengawas_id]',
                          '$pengawas[pengawas_nip]',
                          '$pengawas[pengawas_nama]',
                          '$dateTime',
                          '$peserta[user_id]'
                      )
                  ";

                $stmt2 = $db->prepare($query2);
                $stmt2->execute();
            }
        }

        $result = $db->commit();

        if ($result) {
            $response = array(
                'status' => 201,
                'message' => 'Data berhasil disimpan.',
            );
        }
    } catch (Exception $e) {
        $db->rollback();

        $response = array(
            'status' => 204,
            'message' => $e->getMessage(),
        );
    }


    Flight::json($response);
});

//update jadwal tes eka
Flight::route('PUT /penjadwalan_tes/@id', function ($id) use ($db) {
    $dateTime = date('Y-m-d H:i:s');
    $request = json_decode(Flight::request()->getBody(), true);

    try {
        $db->beginTransaction();

        $updateJadwal = "UPDATE t_jadwal_tes jt 
                SET jt.tanggal_tes = '$request[tanggal]',
                    jt.departemen_id = '$request[departemen_id]', 
                    jt.tanggal_ubah = '$dateTime',
                    jt.user_ubah = '$request[user_id]'
            WHERE id=$id";

        $stmt = $db->prepare($updateJadwal);
        $executed = $stmt->execute();

        if ($executed) {
            foreach ($request['pengawas'] as $pengawas) {
                if ($pengawas['status'] == 'hapus') {
                    $hapusPengawas = "UPDATE t_jadwal_tes_pengawas
                            SET deleted_at = '$dateTime', deleted_by = '$request[user_id]' 
                        WHERE jadwal_tes_id = '$id' AND pengawas_id = '$pengawas[pengawas_id]'";

                    $stmtHapusPengawas = $db->prepare($hapusPengawas);
                    $stmtHapusPengawas->execute();
                } else if ($pengawas['status'] == 'baru') {
                    $insertPengawas = "INSERT INTO t_jadwal_tes_pengawas (
                        jadwal_tes_id, 
                        pengawas_id,
                        pengawas_nip,
                        pengawas_nama, 
                        created_at, 
                        created_by 
                    ) VALUES (
                        '$id',
                        '$pengawas[pengawas_id]',
                        '$pengawas[pengawas_nip]',
                        '$pengawas[pengawas_nama]',
                        '$dateTime',
                        '$request[user_id]'
                    )";

                    $stmtInsertPengawas = $db->prepare($insertPengawas);
                    $stmtInsertPengawas->execute();
                }
            }
        }
        $result = $db->commit();

        if ($result) {
            $response = array(
                'status' => 201,
                'message' => 'Data berhasil disimpan.',
            );
        }
    } catch (Exception $e) {
        $db->rollback();

        $response = array(
            'status' => 204,
            'message' => $e->getMessage(),
        );
    }

    Flight::json($response);
});



/**
 * Route untuk Master 
 * 
 */
Flight::route('GET /master/komponen_nilai', function () use ($db) {
    $user = Flight::request()->query->user;
    $query = "SELECT * FROM m_komponen_nilai";

    if ($user != 0) {
        $query .= " WHERE user_group = '{$user}'";
    }

    $query .= " ORDER BY created_at DESC";

    $stmt = $db->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});

Flight::route('GET /master/komponen_nilai/@id', function ($id) use ($db) {
    $query = "SELECT * FROM m_komponen_nilai WHERE id = '$id'";
    $stmt = $db->prepare($query);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});

Flight::route('POST /master/komponen_nilai', function () use ($db) {
    $request = Flight::request()->data;
    $date = date('Y-m-d H:i:s');

    $query = "INSERT INTO m_komponen_nilai (nama_nilai, bobot_nilai, kelompok_nilai, jenis_penilaian,
              user_group, aktif, created_at, created_by) 
            VALUES ('$request->nama', '$request->bobot', '$request->kelompok', '$request->jenis', 
              '$request->user_group', '$request->status', '$date', '$request->user_id')";

    $stmt = $db->prepare($query);
    $result = $stmt->execute();

    if ($result) {
        $response = array(
            'status' => 201,
            'message' => 'Berhasil menambahkan data.',
        );
    } else {
        $response = array(
            'status' => 204,
            'message' => 'Gagal menambahkan data.'
        );
    }

    Flight::json($response);
});

Flight::route('PUT /master/komponen_nilai/@id', function ($id) use ($db) {
    $request = json_decode(Flight::request()->getBody());
    $date = date('Y-m-d H:i:s');

    $query = "UPDATE 
              m_komponen_nilai
            SET
              nama_nilai = '$request->nama',
              bobot_nilai = '$request->bobot',
              kelompok_nilai = '$request->kelompok',
              jenis_penilaian = '$request->jenis',
              aktif = '$request->status',
              updated_at = '$date',
              updated_by = '$request->user_id'
            WHERE id = '$id'";

    $stmt = $db->prepare($query);
    $result = $stmt->execute();

    if ($result) {
        $response = array(
            'status' => 201,
            'message' => 'Berhasil mengubah data.',
        );
    } else {
        $response = array(
            'status' => 204,
            'message' => 'Gagal mengubah data.'
        );
    }

    Flight::json($response);
});

Flight::route('GET /master/institusi', function () use ($db) {
    $query = "SELECT * FROM m_institusi";

    $stmt = $db->prepare($query);

    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});

Flight::route('GET /master/institusi/@id', function ($id) use ($db) {
    $query = "SELECT * FROM m_institusi WHERE id = '$id'";

    $stmt = $db->prepare($query);

    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});

Flight::route('POST /master/institusi', function () use ($db) {
    $request = Flight::request()->data;
    $date = date('Y-m-d H:i:s');

    $query = "INSERT INTO m_institusi (
    nama,
    contact_person,
    alamat,
    no_telepon,
    email,
    situs,
    aktif,
    created_at,
    created_by
  ) VALUES (
    '$request->nama',
    '$request->contact_person',
    '$request->alamat',
    '$request->no_telepon',
    '$request->email',
    '$request->situs',
    '$request->aktif',
    '$date',
    '$request->user_id'
  )";

    $stmt = $db->prepare($query);
    $result = $stmt->execute();

    if ($result) {
        $response = array(
            'status' => 201,
            'message' => 'Data berhasil disimpan.',
        );
    } else {
        $response = array(
            'status' => 204,
            'message' => 'Gagal menyimpan data.',

        );
    }

    Flight::json($response);
});

Flight::route('PUT /master/institusi/@id', function ($id) use ($db) {
    $request = json_decode(Flight::request()->getBody());
    $date = date('Y-m-d H:i:s');

    $query = "UPDATE m_institusi 
            SET
              aktif = '$request->aktif', 
              nama = '$request->nama',
              contact_person = '$request->contact_person',
              no_telepon = '$request->no_telepon',
              email = '$request->email',
              situs = '$request->situs',
              alamat = '$request->alamat',
              updated_at = '$date',
              updated_by = $request->user_id
              
            WHERE 
              id = $id";

    $stmt = $db->prepare($query);
    $result = $stmt->execute();

    if ($result) {
        $response = array(
            'status' => 201,
            'message' => 'Berhasil mengupdate data.'
        );
    } else {
        $response = array(
            'status' => 204,
            'message' => 'Gagal mengupdate data.'
        );
    }

    Flight::json($response);
});

Flight::route('GET /master/prodi', function () use ($db) {
    $aktif = Flight::request()->query->aktif;
    $query = "SELECT * FROM m_prodi WHERE deleted_at IS NULL";

    if ($aktif) {
        $query .= " AND aktif = {$aktif}";
    }

    $stmt = $db->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});

Flight::route('GET /master/prodi/@id', function ($id) use ($db) {
    $query = "SELECT * FROM m_prodi WHERE id = '$id'";

    $stmt = $db->prepare($query);

    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});

Flight::route('POST /master/prodi', function () use ($db) {
    $request = Flight::request()->data;
    $date = date('Y-m-d H:i:s');
    $query = "INSERT INTO m_prodi (
    nama,
    jenjang,
    aktif,
    created_at,
    created_by
  ) VALUES (
    '$request->nama_prodi',
    '$request->jenjang',
    '$request->aktif',
    '$date',
    '$request->user_id'
  )";

    $stmt = $db->prepare($query);
    $result = $stmt->execute();

    if ($result) {
        $response = array(
            'status' => 201,
            'message' => 'Data berhasil disimpan.',
        );
    } else {
        $response = array(
            'status' => 204,
            'message' => 'Gagal menyimpan data.',

        );
    }

    Flight::json($response);
});

Flight::route('PUT /master/prodi/@id', function ($id) use ($db) {
    $request = json_decode(Flight::request()->getBody());
    $date = date('Y-m-d H:i:s');

    $query = "UPDATE m_prodi 
            SET
              aktif = '$request->aktif', 
              nama = '$request->nama_prodi',
              jenjang = '$request->jenjang',
              updated_at = '$date',
              updated_by = $request->user_id
            WHERE 
              id = $id";

    $stmt = $db->prepare($query);
    $result = $stmt->execute();

    if ($result) {
        $response = array(
            'status' => 201,
            'message' => 'Berhasil mengupdate data.'
        );
    } else {
        $response = array(
            'status' => 204,
            'message' => 'Gagal mengupdate data.'
        );
    }

    Flight::json($response);
});

Flight::route('GET /setting/kuota', function () use ($db) {
    $year = Flight::request()->query->year;
    $jenis = Flight::request()->query->jenis;

    $query = "SELECT * FROM s_kuota_ruangan WHERE tahun = '$year'";


    if (!empty($jenis) && $jenis != 0) {
        $query .= " AND jenis = $jenis";
    }

    $stmt = $db->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});

Flight::route('GET /setting/kuota/@id', function ($id) use ($db) {
    $query = "SELECT * FROM s_kuota_ruangan WHERE id = '$id'";
    $stmt = $db->prepare($query);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});

Flight::route('POST /setting/kuota', function () use ($db) {
    $request = Flight::request()->data;
    $date = date('Y-m-d H:i:s');
    $query = "INSERT INTO s_kuota_ruangan (unit_id, unit_nama, lantai, kuota, tahun,
            jenis, aktif, created_at, created_by
        ) VALUES ('$request->ruangan', '$request->ruangan_nama', '$request->lantai', 
            '$request->kuota', '$request->tahun', '$request->jenis', '1', 
            '$date', '$request->user_id' )";

    $stmt = $db->prepare($query);
    $result = $stmt->execute();

    if ($result) {
        $response = array(
            'status' => 201,
            'message' => 'Data berhasil disimpan.',
        );
    } else {
        $response = array(
            'status' => 204,
            'message' => 'Gagal menyimpan data.',

        );
    }

    Flight::json($response);
});

Flight::route('PUT /setting/kuota/@id', function ($id) use ($db) {
    $request = json_decode(Flight::request()->getBody());
    $date = date('Y-m-d H:i:s');
    $query = "UPDATE s_kuota_ruangan 
              SET kuota = '$request->kuota', updated_at = '$date', updated_by = '$request->updated_by'
              WHERE id = $id";

    $stmt = $db->prepare($query);
    $result = $stmt->execute();

    if ($result) {
        $response = array(
            'status' => 201,
            'message' => 'Berhasil mengupdate data.'
        );
    } else {
        $response = array(
            'status' => 204,
            'message' => 'Gagal mengupdate data.'
        );
    }

    Flight::json($response);
});

Flight::route('DELETE /setting/kuota/@id', function ($id) use ($db) {
    $request = json_decode(Flight::request()->getBody());
    $date = date('Y-m-d H:i:s');
    $query = "UPDATE s_kuota_ruangan 
              SET aktif = 0, deleted_at = '$date', deleted_by = '$request->deleted_by'
              WHERE id = $id";

    $stmt = $db->prepare($query);
    $result = $stmt->execute();

    if ($result) {
        $response = array(
            'status' => 200,
            'message' => 'Berhasil menghapus data.'
        );
    } else {
        $response = array(
            'status' => 400,
            'message' => 'Gagal menghapus data.'
        );
    }

    Flight::json($response);
});

Flight::route('GET /master/unit_rs', function () {
    $connSdm = connectDbSdm();
    $unit = Flight::request()->query['unit'];

    $query = "SELECT a.`idunit`, a.`namaunit`, a.`isunit_aktif` 
            FROM as_ms_unit a 
            WHERE a.`level` 
              NOT IN (1,2,3) 
              AND a.`isunit_aktif` = 1
              AND a.`namaunit` like '%$unit%' LIMIT 5";

    $stmt = $connSdm->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $units = array();

    foreach ($result as $unit) {
        array_push($units, array(
            "id" => $unit['idunit'],
            "text" => $unit['namaunit'],
        ));
    }

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $units
    );

    Flight::json($response);
});

Flight::route('GET /master/departemen', function () use ($db) {
    $jenis = Flight::request()->query->jenis;
    $isAktif = Flight::request()->query->aktif;
    $query = "SELECT * FROM m_departemen WHERE deleted_at IS NULL";

    if (!empty($jenis) && $jenis != 0) {
        $query .= " AND jenis = '$jenis'";
    }

    if (!empty($isAktif) && $isAktif != 0) {
        $query .= " AND aktif = $isAktif";
    }

    $query .= " ORDER BY nama";

    $stmt = $db->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});

Flight::route('GET /master/departemen/@id', function ($id) use ($db) {

    $query = "SELECT * FROM m_departemen WHERE id = '$id'";

    $stmt = $db->prepare($query);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});

Flight::route('POST /master/departemen', function () use ($db) {
    $request = Flight::request()->data;
    $date = date('Y-m-d H:i:s');

    $query = "INSERT INTO m_departemen (nama, jenis, aktif, created_at, created_by) 
              VALUES ('$request->nama', '$request->user_group', '$request->aktif', 
                '$date', '$request->user_id')";

    $stmt = $db->prepare($query);
    $result = $stmt->execute();

    if ($result) {
        $response = array(
            'status' => 201,
            'message' => 'Data berhasil disimpan.',
        );
    } else {
        $response = array(
            'status' => 204,
            'message' => 'Gagal menyimpan data.',

        );
    }

    Flight::json($response);
});

Flight::route('PUT /master/departemen/@id', function ($id) use ($db) {
    $request = json_decode(Flight::request()->getBody());
    $date = date('Y-m-d H:i:s');

    $query = "UPDATE 
                m_departemen 
                SET 
                    aktif = '$request->aktif', 
                    nama = '$request->nama', 
                    jenis = '$request->jenis',
                    updated_at = '$date', 
                    updated_by = '$request->user_id' 
                WHERE id = $id
    ";

    $stmt = $db->prepare($query);
    $result = $stmt->execute();

    if ($result) {
        $response = array(
            'status' => 201,
            'message' => 'Berhasil mengupdate.'
        );
    } else {
        $response = array(
            'status' => 204,
            'message' => 'Gagal mengupdate data.'
        );
    }

    Flight::json($response);
});

include __DIR__ . '/routes/api/master/tarif/index.php';
include __DIR__ . '/routes/api/keuangan/tagihan/index.php';
include __DIR__ . '/routes/api/penjadwalan_praktek/index.php';
include __DIR__ . '/routes/api/penjadwalan_kunjungan/index.php';
include __DIR__ . '/routes/api/penilaian/index.php';

include __DIR__ . '/routes/api/penelitian/master/tarif/index.php';
include __DIR__ . '/routes/api/penelitian/master/pengelola/index.php';

include __DIR__ . '/routes/api/penelitian/internal/penjadwalan/index.php';

include __DIR__ . '/routes/reports/penelitian/tagihan/index.php';

Flight::set('flight.log_errors', true);

Flight::start();
