<?php
//get all atau getby periode => tambah ?periode=YYYY-MM
Flight::route('GET /penjadwalan_praktek/show/@id', function ($id) use ($db) {
    $query = "SELECT
                jp.id,
                p.id AS peserta_id,
                p.nim AS peserta_nim,
                p.nama AS peserta_nama,
                i.id AS institusi_id,
                i.nama AS institusi_nama,
                pr.`id` AS prodi_id,
                pr.nama AS prodi_nama,
                jp.departemen_id AS departemen_id,
                d.`nama` AS departemen_nama,
                jp.ruangan_id AS ruangan_id,
                jp.ruangan_nama,
                jp.pegawai_id AS pembimbing_id,
                jp.pegawai_nama AS pembimbing_nama,
                jp.tgl_mulai,
                jp.tgl_selesai,
                jp.status
            FROM
                t_jadwal_praktek jp
                LEFT JOIN m_peserta_didik p
                ON jp.peserta_didik_id = p.id
                LEFT JOIN m_institusi i
                ON p.institusi_id = i.id
                LEFT JOIN m_prodi pr
                ON pr.id = p.prodi_id
                LEFT JOIN `m_departemen` d
                ON jp.`departemen_id` = d.`id`
            WHERE jp.id = $id";

    $stmt = $db->prepare($query);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    if (!$result) {
        $response = array(
            'status' => 404,
            'message' => 'No Data Available',
        );
    }

    Flight::json($response);
});

Flight::route('POST /penjadwalan_praktek', function () use ($db) {
    $request = json_decode(Flight::request()->getBody());

    $date = date('Y-m-d H:i:s');

    try {
        $db->beginTransaction();


        foreach ($request as $data) {
            if (empty($data->departemen_id)) {
                throw new Exception('Departemen belum dipilih.');
            }

            if (empty($data->ruangan_id)) {
                throw new Exception('Ruangan belum dipilih.');
            }

            if (empty($data->pegawai_id)) {
                throw new Exception('Pembimbing belum dipilih.');
            }

            if (empty($data->tgl_mulai) or empty($data->tgl_selesai)) {
                throw new Exception('Tanggal mulai atau tanggal selesai belum dipilih.');
            }

            $qcek_duplicate = "SELECT mpd.id,
                                       UPPER(mpd.nama) AS nama
                                FROM t_jadwal_praktek tjp
                                         INNER JOIN m_peserta_didik mpd
                                                    ON mpd.id = tjp.peserta_didik_id
                                WHERE tjp.peserta_didik_id = '$data->peserta_id'
                                  AND (tjp.tgl_mulai = '$data->tgl_mulai'
                                    AND tjp.tgl_selesai = '$data->tgl_selesai')
                                  AND tjp.deleted_at IS NULL;";
            $stmt = $db->prepare($qcek_duplicate);
            $stmt->execute();

            if ($stmt->rowCount() > 0) {
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                throw new Exception("Peserta Didik {$row['nama']} Sudah Memiliki Jadwal Ditanggal Tersebut!!.");
            }

            $query = "INSERT INTO t_jadwal_praktek (
                peserta_didik_id,
                departemen_id,
                ruangan_id,
                ruangan_nama,
                pegawai_id,    
                pegawai_nama,    
                tgl_mulai,
                tgl_selesai,
                created_at,
                created_by
            )
            VALUES (
                '$data->peserta_id',
                '$data->departemen_id',
                '$data->ruangan_id',
                '$data->ruangan_nama', 
                '$data->pegawai_id',
                '$data->pegawai_nama',
                '$data->tgl_mulai',
                '$data->tgl_selesai',
                '$date',
                '$data->user_id'
            )";

            $stmt = $db->prepare($query);
            $stmt->execute();
            $last_id = $db->lastInsertId();

            $qs = "SELECT
                            pegawai_id,
                            UPPER(pegawai_nama) AS pegawai_nama,
                            UPPER(jenis) AS jenis,
                            `status`
                        FROM
                            `m_instalasi_komkordik`
                        WHERE `status` = 1
                            AND `deleted_at` IS NULL
                            AND `deleted_by` IS NULL;";
            $stmtins = $db->prepare($qs);
            $stmtins->execute();
            if ($stmtins->rowCount() > 0) {
                while ($row = $stmtins->fetch(PDO::FETCH_ASSOC)) {
                    extract($row);
                    $qi = "INSERT INTO 
                                `t_jadwal_instalasi_komkordik` 
                            SET 
                                jadwal_praktek_id = $last_id,
                                pegawai_id = $pegawai_id,
                                pegawai_nama = '$pegawai_nama',
                                jenis = LOWER('$jenis');";
                    $stmti = $db->prepare($qi);
                    $stmti->execute();
                }
            }
        }

        $db->commit();

        $response = array(
            'status' => 201,
            'message' => 'berhasil menambahkan data.'
        );
    } catch (Exception $e) {
        $db->rollBack();
        $response = array(
            'status' => 204,
            'message' => $e->getMessage(),
        );
    }

    return Flight::json($response);
});

Flight::route('PUT /penjadwalan_praktek/@id', function ($id) use ($db) {
    $request = json_decode(Flight::request()->getBody());
    try {

        $ruangan_id = $request->ruangan_id;
        $ruangan_nama = $request->ruangan_nama;
        $departemen_id = $request->departemen_id;
        $pegawai_id = $request->pembimbing_id;
        $pegawai_nama = $request->pembimbing_nama;
        $tgl_mulai = $request->tgl_mulai;
        $tgl_selesai = $request->tgl_selesai;
        $user_id = $request->user_id;
        $date = date('Y-m-d H:i:s');

        $db->beginTransaction();

        $query = "UPDATE
                    t_jadwal_praktek
                SET
                    departemen_id = '$departemen_id',
                    ruangan_id = '$ruangan_id',
                    ruangan_nama = '$ruangan_nama',
                    pegawai_id = '$pegawai_id',
                    pegawai_nama = '$pegawai_nama',
                    tgl_mulai = '$tgl_mulai',
                    tgl_selesai = '$tgl_selesai',
                    updated_at = '$date',
                    updated_by = '$user_id'
                WHERE
                    id = $id";
        $stmt = $db->prepare($query);
        $result = $stmt->execute();

        if (!$result) {
            throw new Exception('Update Data failed!!');
        }

        $query = "UPDATE
                    `t_penilaian`
                    SET
                    `departemen_id` = $departemen_id
                    WHERE `jadwal_id` = $id
                    AND (`jenis_penilaian` = 3 OR `jenis_penilaian` = 4)";
        $stmt = $db->prepare($query);
        $result = $stmt->execute();

        if (!$result) {
            throw new Exception('Update Data failed!!');
        }

        $db->commit();
        $response = array(
            'status' => 200,
            'message' => 'Data has been updated.'
        );
    } catch (Exception $e) {
        $db->rollback();
        $response = array(
            'status' => 204,
            'message' => $e->getMessage()
        );
    }

    Flight::json($response);
});

Flight::route('GET /penjadwalan_praktek/departemen', function () use ($db) {
    $query = "SELECT * FROM m_departemen WHERE aktif = 1";

    $stmt = $db->prepare($query);

    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});

Flight::route('GET /penjadwalan_praktek/cari/mahasiswa/@institusi/@prodi', function ($institusi, $prodi) use ($db) {
    $query = "SELECT id, nama, nim, nik FROM m_peserta_didik where institusi_id = $institusi AND aktif = 1 AND prodi_id = $prodi";
    $stmt = $db->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});

Flight::route('GET /penjadwalan_praktek', function () use ($db) {
    $user = Flight::request()->query->user;
    $periode = Flight::request()->query->periode;

    $query = "SELECT pp.id, pd.nik, pd.nim, pd.nama AS nama_peserta, d.nama AS nama_departemen,
                  p.nama AS nama_prodi, i.nama AS nama_institusi, pp.tgl_mulai, pp.tgl_selesai,
                  pp.ruangan_id, pp.ruangan_nama, pp.status
                FROM t_jadwal_praktek pp
                  INNER JOIN m_peserta_didik pd ON pp.peserta_didik_id = pd.id
                  INNER JOIN m_departemen d ON pp.departemen_id = d.id
                  INNER JOIN m_prodi p ON pd.prodi_id = p.id
                  INNER JOIN m_institusi i ON pd.institusi_id = i.id
                WHERE pp.deleted_at IS NULL";

    if ($periode) {
        $query .= " AND pp.tgl_selesai LIKE '{$periode}%'";
    }

    if (!empty($user) && $user != 0) {
        $query .= " AND pd.jenis = '$user'";
    }

    $query .= " ORDER BY id DESC";

    $stmt = $db->prepare($query);

    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});

Flight::route('GET /penjadwalan_praktek/institusi', function () use ($db) {

    $query = "SELECT * FROM m_institusi WHERE aktif = 1";

    $stmt = $db->prepare($query);

    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});

Flight::route('GET /penjadwalan_praktek/ruangan/@year', function ($year) use ($db) {
    $query = "SELECT * FROM s_kuota_ruangan WHERE tahun = '$year' and aktif =1";
    $stmt = $db->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});

Flight::route('PUT  /penjadwalan_praktek/@id/destroy', function ($id) use ($db) {
    $request = json_decode(Flight::request()->getBody(), TRUE);
    $userId = $request['user_id'];
    $today = date('Y-m-d H:i:s');

    try {
        if (!$id) {
            throw new Exception("Data penjadwalan tidak ditemukan", 422);
        }

        $row = $db->exec("UPDATE t_jadwal_praktek a SET a.status = '0', a.deleted_at = '$today', a.deleted_by = '$userId' WHERE a.id = '$id'");

        $response = array(
            'status' => 200,
            'message' => 'Data berhasil di hapus.',
            'affected_row' => $row
        );
    } catch (\Throwable $th) {
        $response = array(
            'status' => 422,
            'message' => $th->getMessage()
        );
    }

    Flight::json($response, $response['status']);
});
