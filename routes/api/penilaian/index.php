<?php
Flight::route('GET /penilaian', function () use ($db) {
    $user = Flight::request()->query->user;

    $query = "SELECT 
                  p.id AS penilaian_id, 
                  p.jadwal_id,
                  p.peserta_didik_id, 
                  pd.nim AS nim_peserta, 
                  pd.nama AS nama_peserta,
                  i.nama AS institusi, 
                  pr.nama AS prodi, 
                  p.jenis_penilaian,
                  d.id AS departemen_id,
                  d.nama AS departemen_nama, 
                  p.penilai, 
                  p.penilai_id,
              CASE
                  WHEN pd.jenis = 1 THEN 'Keperawatan'
                  WHEN pd.jenis = 2 THEN 'Penunjang'
                  ELSE 'Undefined'
              END
                  AS owned_by
              FROM t_penilaian p
                INNER JOIN m_peserta_didik pd ON pd.id = p.peserta_didik_id
                LEFT JOIN m_institusi i ON i.id = pd.institusi_id
                LEFT JOIN m_departemen d ON d.id = p.departemen_id
                LEFT JOIN m_prodi pr ON pr.id = pd.prodi_id
      ";

    if (!empty($user) && $user != 0) {
        $query .= " WHERE p.user_group = '$user'";
    }

    $query .= ' GROUP BY p.peserta_didik_id, p.jenis_penilaian, p.departemen_id, p.jadwal_id ORDER BY p.created_at DESC';
    $stmt = $db->prepare($query);

    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});

Flight::route('GET /penilaian/komponen_nilai', function () use ($db) {
    $jenis = Flight::request()->query->jenis;
    $group = Flight::request()->query->group;

    $query = "SELECT * FROM m_komponen_nilai WHERE jenis_penilaian = $jenis";

    if ($group != 0) {
        $query .= " AND user_group = '$group'";
    }

    $stmt = $db->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});

Flight::route('GET /penilaian/cari_mahasiswa/@group/@jenis(/@departemen)', function ($group, $jenis, $departemen) use ($db) {
    $nama = Flight::request()->query['nama'];

    if ($jenis < 3) {
        $table = "t_jadwal_tes";
        $jadwal = "j.tanggal_tes AS jadwal_tanggal";
        $tes = " AND j.jenis_tes = '$jenis'";
        $department = "";
    } else {
        $table = "t_jadwal_praktek";
        $jadwal = "j.tgl_mulai AS mulai, j.tgl_selesai AS selesai";
        $department = "WHERE j.departemen_id = '$departemen' ";
        $tes = "";
    }

    $query = "SELECT 
                      pd.id, pd.nim, 
                      pd.nama, j.id AS jadwal_id, 
                      $jadwal,
                  CASE
                      WHEN pd.jenis = 1 THEN 'Keperawatan'
                      WHEN pd.jenis = 2 THEN 'Penunjang'
                      ELSE 'Undefined'
                  END
                      AS owned_by
              FROM 
                  m_peserta_didik pd INNER JOIN $table j ON j.peserta_didik_id = pd.id
              WHERE pd.id IN (SELECT peserta_didik_id FROM $table $department)
                $tes";

    if ($group != 0) {
        $query .= " AND pd.jenis = '$group'";
    }

    if ($nama) {
        $query .= " AND pd.nama LIKE '%$nama%'";
    }
    // var_dump($query); die;
    $stmt = $db->prepare($query);

    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if (!empty($result)) {
        $response = array(
            'status' => 200,
            'message' => 'success',
            'data' => $result
        );
    } else {
        $response = array(
            'status' => 404,
            'message' => 'Data not found',
        );
    }

    Flight::json($response);
});

Flight::route('GET /penilaian/@peserta_didik_id/@jenis_penilaian/@departemen/@jadwal_id', function ($id, $jenis, $departemen, $jadwal_id) use ($db) {
    $query = "SELECT p.id, p.peserta_didik_id AS peserta_id, pd.nama AS peserta_nama,
                  p.komponen_nilai_id AS nilai_id, p.komponen_nilai_nama AS nilai_nama,
                  p.penilai_id, p.penilai, p.jenis_penilaian, p.departemen_id,
                  IF (p.departemen_id != 0, d.nama, NULL) AS departemen,
                  p.komponen_nilai_id, p.komponen_nilai_nama, p.nilai
              FROM
                  t_penilaian p
                  INNER JOIN m_peserta_didik pd
                  ON pd.id = p.peserta_didik_id
                  LEFT JOIN m_departemen d
                  ON d.id = p.departemen_id
              WHERE peserta_didik_id = '$id'
                  AND jenis_penilaian = '$jenis'
                  AND departemen_id = '$departemen'
                  AND jadwal_id = '$jadwal_id'
      ";
    // var_dump($query);die;
    $stmt = $db->prepare($query);

    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $data = array(
        'jenis_penilaian' => $result[0]['jenis_penilaian'],
        'departemen' => $result[0]['departemen_id'],
        'penilai_id' => $result[0]['penilai_id'],
        'penilai' => $result[0]['penilai'],
        'peserta_id' => $result[0]['peserta_id'],
        'peserta_nama' => $result[0]['peserta_nama'],
        'nilai' => array()

    );

    foreach ($result as $nilai) {
        array_push($data['nilai'], array(
            "nilai_id" => $nilai['komponen_nilai_id'],
            "nilai_nama" => $nilai['komponen_nilai_nama'],
            "nilai" => $nilai['nilai'],
            "penilaian_id" => $nilai['id'],
        ));
    }

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $data
    );

    Flight::json($response);
});

Flight::route('GET /penilaian/@id/@jenis(/@departemen)', function ($id, $jenis, $departemen) use ($db) {
    $query = "SELECT p.id, p.peserta_didik_id AS peserta_id, pd.nama AS peserta_nama,
                  p.komponen_nilai_id AS nilai_id, p.komponen_nilai_nama AS nilai_nama,
                  p.penilai_id, p.penilai, p.jenis_penilaian, p.departemen_id,
                  IF (p.departemen_id != 0, d.nama, NULL) AS departemen,
                  p.komponen_nilai_id, p.komponen_nilai_nama, p.nilai
              FROM
                  t_penilaian p
                  INNER JOIN m_peserta_didik pd
                  ON pd.id = p.peserta_didik_id
                  LEFT JOIN m_departemen d
                  ON d.id = p.departemen_id
              WHERE peserta_didik_id = '$id'
                  AND jenis_penilaian = '$jenis'
                  AND departemen_id = '$departemen'
      ";

    $stmt = $db->prepare($query);

    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $data = array(
        'jenis_penilaian' => $result[0]['jenis_penilaian'],
        'departemen' => $result[0]['departemen_id'],
        'penilai_id' => $result[0]['penilai_id'],
        'penilai' => $result[0]['penilai'],
        'peserta_id' => $result[0]['peserta_id'],
        'peserta_nama' => $result[0]['peserta_nama'],
        'nilai' => array()

    );

    foreach ($result as $nilai) {
        array_push($data['nilai'], array(
            "nilai_id" => $nilai['komponen_nilai_id'],
            "nilai_nama" => $nilai['komponen_nilai_nama'],
            "nilai" => $nilai['nilai'],
            "penilaian_id" => $nilai['id'],
        ));
    }

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $data
    );

    Flight::json($response);
});

Flight::route('POST /penilaian', function () use ($db) {
    $request = Flight::request()->data;
    $date = date('Y-m-d H:i:s');

    try {
        $db->beginTransaction();

        $queryJadwal = "SELECT COUNT(id) AS count_penilaian FROM t_penilaian 
                          WHERE jadwal_id = '$request->penjadwalan_id' 
                              AND peserta_didik_id = '$request->peserta_didik_id'
                              AND jenis_penilaian = '$request->jenis_penilaian'";
        $getJadwal = $db->query($queryJadwal);
        $countJadwal = (int) $getJadwal->fetchColumn();

        if ($countJadwal > 0) {
            throw new Exception('Penjadwalan ini sudah dinilai.');
        }

        if (empty($request->penilai_id)) {
            throw new Exception('Penilai tidak ada.');
        }

        foreach ($request['nilai'] as $nilai) {
            $query = "INSERT INTO t_penilaian 
            (
              komponen_nilai_id,
              komponen_nilai_nama,
              nilai,
              jadwal_id, 
              peserta_didik_id,
              jenis_penilaian,
              departemen_id,
              penilai_id,
              penilai,
              user_group,
              created_at,
              created_by
            )
            VALUES 
            (
              '$nilai[komponen_nilai_id]',
              '$nilai[komponen_nilai_nama]',
              '$nilai[nilai]',
              '$request->penjadwalan_id',
              '$request->peserta_didik_id',
              '$request->jenis_penilaian',
              '$request->departemen_id',
              '$request->penilai_id',
              '$request->penilai',
              '$request->user_group',
              '$date',
              '$request->created_by'
            )";

            $stmt = $db->prepare($query);
            $stmt->execute();
        }
        $result = $db->commit();

        if ($result) {
            $response = array(
                'status' => 201,
                'message' => 'Data berhasil disimpan.',
            );
        }
    } catch (Exception $e) {
        $response = array(
            'status' => 204,
            'message' => $e->getMessage(),
        );

        $db->rollback();
    }

    return Flight::json($response);
});

Flight::route('PUT /penilaian', function () use ($db) {
    $request = json_decode(Flight::request()->getBody(), true);
    $dateTime = date('Y-m-d H:i:s');

    try {
        $db->beginTransaction();

        foreach ($request['nilai'] as $nilai) {
            $query = "UPDATE t_penilaian SET nilai = '$nilai[nilai]', penilai_id = '$request[penilai_id]',
                    penilai = '$request[penilai]', updated_at = '$dateTime', updated_by = '$request[user_id]' 
                WHERE id = '$nilai[penilaian_id]'";

            $stmt = $db->prepare($query);
            $stmt->execute();
        }

        $result = $db->commit();

        if ($result) {
            $response = array(
                'status' => 201,
                'message' => 'Data berhasil disimpan.',
            );
        }
    } catch (Exception $e) {
        $response = array(
            'status' => 204,
            'message' => $e->getMessage(),
        );

        $db->rollBack();
    }
    Flight::json($response);
});
