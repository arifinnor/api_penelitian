<?php
// Register db class
Flight::register(
  'db',
  'PDO',
  array('mysql:host=localhost;dbname=pendidikan_rs', 'root', ''),
  // array('mysql:host=10.10.1.105;dbname=pendidikan_rs', 'root', 'rsudsda'),
  function ($db) {
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->setAttribute(PDO::ATTR_TIMEOUT, 5);
  }
);

// Register db2 class
Flight::register(
  'db2',
  'PDO',
  array('mysql:host=localhost;dbname=db_pendidikan_nondm', 'root', ''),
  function ($db) {
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->setAttribute(PDO::ATTR_TIMEOUT, 5);
  }
);

function connectDbPendidikan()
{
  try {
    $host = 'localhost';
    $user = 'root';
    $password = '';
    $database = 'db_rspendidikan';
    $dsn = "mysql:host=$host;dbname=$database";
    $connection = new PDO($dsn, $user, $password);

    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $connection->setAttribute(PDO::ATTR_TIMEOUT, 5);

    return  $connection;
  } catch (PDOException $e) {
    die("Database connection failed: " . $e->getMessage());
  }
};

function connectDbSdm()
{
  try {
    $host = '10.10.1.105';
    $user = 'root';
    $password = '1t@2022_rsud';
    $database = 'sdm';
    $method = "mysql:host=$host;dbname=$database";
    $connection = new PDO($method, $user, $password);

    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $connection->setAttribute(PDO::ATTR_TIMEOUT, 5);

    return  $connection;
  } catch (PDOException $e) {
    die("Database connection failed: " . $e->getMessage());
  }
}


function http_request($url)
{
  $ch = curl_init(); // initiasi curl
  curl_setopt($ch, CURLOPT_URL, $url); // set url
  curl_setopt($ch, CURLOPT_FAILONERROR, true); // Required for HTTP error codes to be reported via our call to curl_error($ch)
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return the transfer as a string 

  // $output contains the output string 
  $output = curl_exec($ch);

  if (!$output) {
    $error_msg = curl_error($ch);
  }

  curl_close($ch); // close curl

  return $output;
}
