<?php

//get all atau getby periode => tambah ?periode=YYYY-MM
Flight::route('GET /penjadwalan_kunjungan', function () use ($db) {
    $periode =  Flight::request()->query->periode; //format Y-m
    if (empty($periode)) {
        $periode = date('Y-m');
    }

    $query = "SELECT
                    jk.id,
                    p.id AS id_peserta,
                    p.nim AS nim_peserta,
                    p.nama AS nama_peserta,
                    i.nama AS institusi,
                    pr.nama AS prodi,
                    jk.ruangan_id,
                    jk.ruangan_nama,
                    jk.pegawai_id,
                    jk.pegawai_nama,
                    jk.tgl_mulai,
                    jk.tgl_selesai,
                    jk.status
                FROM
                    t_jadwal_kunjungan jk
                    LEFT JOIN m_peserta_didik p ON jk.peserta_didik_id = p.id
                    LEFT JOIN m_institusi i ON p.institusi_id = i.id
                    LEFT JOIN m_prodi pr ON pr.id = p.prodi_id
                WHERE 
                    tgl_selesai LIKE '{$periode}%'
                    AND jk.status = 1
                    AND jk.deleted_at IS NULL";

    $stmt = $db->prepare($query);

    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    Flight::json($response);
});


Flight::route('POST /penjadwalan_kunjungan/', function () use ($db) {
    $request = Flight::request()->data;
    $date = date('Y-m-d H:i:s');
    try {
        $db->beginTransaction();

        foreach ($request as $data) {
            if (empty($data['ruangan_id'])) {
                throw new Exception('Ruangan belum dipilih.');
            }

            if (empty($data['pegawai_id'])) {
                throw new Exception('Pembimbing belum dipilih.');
            }

            if (empty($data['tgl_mulai']) or empty($data['tgl_selesai'])) {
                throw new Exception('Tanggal mulai atau tanggal selesai belum dipilih.');
            }

            if (date("Y-m-d", strtotime($data['tgl_selesai'])) < date("Y-m-d", strtotime($data['tgl_mulai']))) {
                throw new Exception('Tanggal mulai harus lebih besar dari tanggal selesai!!');
            }

            $query = "SELECT
                            *
                        FROM
                            t_jadwal_kunjungan
                        WHERE
                            peserta_didik_id = {$data['peserta_didik_id']}
                            AND ruangan_id = {$data['ruangan_id']}
                            AND pegawai_id = {$data['pegawai_id']}
                            AND (
                                (
                                    tgl_mulai BETWEEN '{$data['tgl_mulai']}' AND '{$data['tgl_selesai']}'
                                OR
                                    tgl_selesai BETWEEN '{$data['tgl_mulai']}' AND '{$data['tgl_selesai']}'
                                    
                                )
                            )
                            AND status = 1
                            AND deleted_at IS NULL";
            // var_dump($query);
            // die;
            $stmt = $db->prepare($query);
            $stmt->execute();

            if ($stmt->rowCount() > 0) {
                throw new Exception('Data Jadwal Kunjungan Sudah Ada!!');
            }

            $query = "INSERT INTO t_jadwal_kunjungan (
                    peserta_didik_id,
                    ruangan_id,
                    ruangan_nama,
                    pegawai_id,
                    pegawai_nama,
                    tgl_mulai,
                    tgl_selesai,
                    created_at,
                    created_by
                )
                VALUES (
                    '{$data['peserta_didik_id']}',
                    '{$data['ruangan_id']}',
                    '{$data['ruangan_nama']}', 
                    '{$data['pegawai_id']}',
                    '{$data['pegawai_nama']}',
                    '{$data['tgl_mulai']}',
                    '{$data['tgl_selesai']}',
                    '$date',
                    '{$data['user_id']}'
                )";

            $stmt = $db->prepare($query);
            $stmt->execute();
        }

        $db->commit();

        $response = array(
            'status' => 201,
            'message' => 'berhasil menambahkan data.'
        );
    } catch (Exception $e) {
        $db->rollBack();
        $response = array(
            'status' => 422,
            'message' => $e->getMessage(),
        );
    }

    return Flight::json($response);
});

Flight::route('GET /penjadwalan_kunjungan/@id', function ($id) use ($db) {
    $query = "SELECT
                    jk.id,
                    p.id AS id_peserta,
                    p.nim AS nim_peserta,
                    p.nama AS nama_peserta,
                    i.nama AS institusi,
                    pr.nama AS prodi,
                    jk.ruangan_id,
                    jk.ruangan_nama,
                    jk.pegawai_id,
                    jk.pegawai_nama,
                    jk.tgl_mulai,
                    jk.tgl_selesai,
                    jk.status
                FROM
                    t_jadwal_kunjungan jk
                    LEFT JOIN m_peserta_didik p ON jk.peserta_didik_id = p.id
                    LEFT JOIN m_institusi i ON p.institusi_id = i.id
                    LEFT JOIN m_prodi pr ON pr.id = p.prodi_id 
                WHERE 
                    jk.id = $id
                ";

    $stmt = $db->prepare($query);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);

    $response = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    if (!$result) {
        $response = array(
            'status' => 404,
            'message' => 'No Data Available',
        );
    }

    Flight::json($response);
});

Flight::route('PUT /penjadwalan_kunjungan/@id', function ($id) use ($db) {
    $request = json_decode(Flight::request()->getBody());
    try {

        $peserta_didik_id = $request->peserta_didik_id;
        $ruangan_id = $request->ruangan_id;
        $ruangan_nama = $request->ruangan_nama;
        $pegawai_id = $request->pegawai_id;
        $pegawai_nama = $request->pegawai_nama;
        $tgl_mulai = $request->tgl_mulai;
        $tgl_selesai = $request->tgl_selesai;
        $status = $request->status;
        $user_id = $request->user_id;



        $date = date('Y-m-d H:i:s');

        $query = "UPDATE
                    t_jadwal_kunjungan
                SET
                    peserta_didik_id = '$peserta_didik_id',
                    ruangan_id = '$ruangan_id',
                    ruangan_nama = '$ruangan_nama',
                    pegawai_id = '$pegawai_id',
                    pegawai_nama = '$pegawai_nama',
                    tgl_mulai = '$tgl_mulai',
                    tgl_selesai = '$tgl_selesai',
                    `status` = '$status',
                    updated_at = '$date',
                    updated_by = '$user_id'
                WHERE
                    id = $id";
        $stmt = $db->prepare($query);
        $result = $stmt->execute();
        if (!$result) {
            throw new Exception('Update Data failed!!');
        }
        $response = array(
            'status' => 200,
            'message' => 'Data has been updated.'
        );
    } catch (Exception $e) {
        $response = array(
            'status' => 204,
            'message' => $e->getMessage()
        );
    }

    Flight::json($response);
});

Flight::route('GET /penjadwalan_kunjungan/cari/mahasiswa', function () use ($db) {
    $ruangan =  Flight::request()->query->ruangan; //id
    $pegawai_id =  Flight::request()->query->pegawai_id; //id
    $institusi =  Flight::request()->query->institusi; //id
    $prodi =  Flight::request()->query->prodi; //id
    $tgl_mulai =  Flight::request()->query->tgl_mulai; //Y-m-d
    $tgl_selesai =  Flight::request()->query->tgl_selesai; //Y-m-d
    $idpeserta = "";
    try {
        //code...

        $query = "SELECT
                   peserta_didik_id
               FROM
                   t_jadwal_kunjungan
               WHERE
                   ruangan_id = {$ruangan}
                   AND pegawai_id = {$pegawai_id}
                   AND (
                       (
                           tgl_mulai BETWEEN '{$tgl_mulai}' AND '{$tgl_selesai}'
                       OR
                           tgl_selesai BETWEEN '{$tgl_mulai}' AND '{$tgl_selesai}'
                           
                       )
                   )
                   AND status = 1
                   AND deleted_at IS NULL";
        // var_dump($query);
        // die;
        $stmt = $db->prepare($query);
        $stmt->execute();

        while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $idpeserta .= $result['peserta_didik_id'] . ",";
        }
        $idpeserta = substr($idpeserta, 0, -1);

        if (empty($idpeserta)) {
            $idpeserta = "''";
        }

        $query = "SELECT
                    id,
                    nama,
                    nim,
                    nik
                FROM
                    m_peserta_didik
                where
                    institusi_id = $institusi
                    AND aktif = 1
                    AND prodi_id = $prodi
                    AND id NOT IN($idpeserta)";
        // var_dump($query); die;
        $stmt = $db->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $response = array(
            'status' => 200,
            'message' => 'success',
            'data' => $result
        );
    } catch (Exception $e) {
        $response = array(
            'status' => 200,
            'message' => $e->getMessage(),
            'data' => array()
        );
    }
    Flight::json($response);
});

Flight::route('GET /keuangan/tagihan/peserta_didik/kunjungan/@institusi', function ($institusi) use ($db) {
    $query = "SELECT
            pd.`id` AS peserta_didik_id,
            pd.`nik`,
            pd.`nim`,
            pd.`nama` AS nama_peserta_didik,
            jk.`ruangan_id`,
            jk.`ruangan_nama`,
            jk.`pegawai_id`,
            jk.`pegawai_nama`,
            jk.`tgl_mulai`,
            jk.`tgl_selesai`,
            COALESCE(mt.`nilai_tarif`, 0) AS nilai_tarif
        FROM
            `t_jadwal_kunjungan` jk
            LEFT JOIN `m_peserta_didik` pd ON jk.`peserta_didik_id` = pd.`id`
            LEFT JOIN `m_institusi` i ON i.`id` = pd.`institusi_id`
            LEFT JOIN `m_prodi` pr ON pr.`id` = pd.`prodi_id`
            LEFT JOIN `m_tarif` mt ON pr.`id` = mt.`prodi_id` AND mt.`jenis_tarif` = 5
        WHERE i.`id` = $institusi
        ORDER BY pd.`nama`
    ";

    $stmt = $db->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if ($result) {
        $response = array(
            'status' => 200,
            'message' => 'success',
            'data' => $result
        );
    } else {
        $response = array(
            'status' => 404,
            'message' => 'Data not found!',
        );
    }

    Flight::json($response);
});
