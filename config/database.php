<?php

namespace config\Database;

use PDO;
use PDOException;

/**
 * Database connection configurations
 */
class Database
{
  protected string $host, $user, $password, $db;
  public PDO $connection;

  public function __construct($host = 'localhost', $user = 'root', $password = '', $db = "pendidikan_rs")
  {
    $this->host = $host;
    $this->user = $user;
    $this->password = $password;
    $this->db = $db;
  }

  public function getConnection($host, $user, $password, $db)
  {
    try {
      $dsn = "mysql:host=$host;dbname=$db";
      $connection = new PDO($dsn, $user, $password);
      $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $connection->setAttribute(PDO::ATTR_TIMEOUT, 5);

      $this->connection = $connection;
    } catch (PDOException $e) {
      echo 'error : ' . $e->getMessage();
    }

    return $this->connection;
  }

  public function setHost($host)
  {
    $this->host = $host;

    return $this;
  }

  public function setUser($user)
  {
    $this->user = $user;

    return $this;
  }

  public function setPassword($password)
  {
    $this->password = $password;

    return $this;
  }

  public function setDbName($db)
  {
    $this->db = $db;

    return $this;
  }
}
